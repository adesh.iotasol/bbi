﻿CREATE TABLE [dbo].[Event] (
    [Id]             BIGINT             IDENTITY (1, 1) NOT NULL,
    [CreatedBy]      BIGINT             NOT NULL,
    [UpdatedBy]      BIGINT             NOT NULL,
    [CreatedOn]      DATETIMEOFFSET (7) CONSTRAINT [DF_Event_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [UpdatedOn]      DATETIMEOFFSET (7) CONSTRAINT [DF_Event_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [IsDeleted]      BIT                CONSTRAINT [DF_Event_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsActive]       BIT                CONSTRAINT [DF_Event_IsActive] DEFAULT ((1)) NOT NULL,
    [Title]          NVARCHAR (255)     NOT NULL,
    [Description]    NVARCHAR (MAX)     NULL,
    [StartDate]      DATETIMEOFFSET (7) CONSTRAINT [DF_Event_StartDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [EndDate]        DATETIMEOFFSET (7) CONSTRAINT [DF_Event_EndDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [EventbriteLink] NVARCHAR (MAX)     NULL,
    [VideoPath]      NVARCHAR (MAX)     NULL,
    [Address]        NVARCHAR (MAX)     NULL,
    [EventTime]      NVARCHAR (100)     NULL,
    CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED ([Id] ASC)
);







