﻿CREATE TABLE [dbo].[Contact] (
    [Id]        BIGINT             IDENTITY (1, 1) NOT NULL,
    [CreatedOn] DATETIMEOFFSET (7) CONSTRAINT [DF_Contact_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
    [Name]      NVARCHAR (200)     NULL,
    [Email]     NCHAR (250)        NOT NULL,
    [Subject]   NCHAR (250)        NULL,
    [Message]   NVARCHAR (MAX)     NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([Id] ASC)
);

