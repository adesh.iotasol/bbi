﻿CREATE PROCEDURE [dbo].[ContactSelect](
@Id	        BIGINT =NULL
)
AS BEGIN

 SELECT * from Contact WHERE 
	(
		@Id IS NULL
		OR
		Id = @Id
	)
	Order by Id desc

END