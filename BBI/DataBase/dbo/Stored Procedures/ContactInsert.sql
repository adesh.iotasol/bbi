﻿CREATE Procedure [dbo].[ContactInsert](
@Name NVARCHAR(MAX) = NULL,
@Email NVARCHAR(250) = NULL,
@Subject NVARCHAR(250) = NULL,
@Message NVARCHAR(MAX) = NULL
)
AS
BEGIN 
	INSERT INTO Contact(
		Name,
		Email,
		[Subject],
		[Message]
	
	)
	VALUES(
		@Name,
		@Email,
		@Subject,
		@Message
	)
	END