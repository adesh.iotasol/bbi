﻿CREATE PROCEDURE [dbo].[CategoryXMLSave]
 @CategoryXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name'
  FROM 
	@CategoryXml.nodes('/root/Category') AS NDS(DT)
   )
   MERGE INTO Category R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.CreatedBy =ISNULL(x.CreatedBy ,R.CreatedBy),R.UpdatedBy =ISNULL(x.UpdatedBy ,R.UpdatedBy),R.CreatedOn =ISNULL(x.CreatedOn ,R.CreatedOn),R.UpdatedOn =ISNULL(x.UpdatedOn ,R.UpdatedOn),R.IsDeleted =ISNULL(x.IsDeleted ,R.IsDeleted),R.IsActive =ISNULL(x.IsActive ,R.IsActive),R.Name =ISNULL(x.Name ,R.Name)
  WHEN NOT MATCHED 
  THEN 
    INSERT
    VALUES(
    x.CreatedBy,x.UpdatedBy,x.CreatedOn,x.UpdatedOn,x.IsDeleted,x.IsActive,x.Name
    );
END