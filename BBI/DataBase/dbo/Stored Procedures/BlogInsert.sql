﻿
CREATE PROCEDURE [dbo].[BlogInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
	    @CategoryId BIGINT=NULL,
		@Slug     NVARCHAR(500)=NULL,
		@VideoPath NVARCHAR(MAX)= NULL,
		@HomePageActive BIT = NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Blog]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[CategoryId],[Slug],[VideoPath],[HomePageActive]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@CategoryId,@Slug,@VideoPath,@HomePageActive
	  )
	SELECT SCOPE_IDENTITY()
 END