﻿CREATE PROCEDURE [dbo].[BlogSelect](
@Id	        BIGINT =NULL,
@FileType NVARCHAR(50),
@IsActive bit = NULL,
@next int = NULL,
@offset int = NULL,
@Slug    NVARCHAR(500)=NULL,
@CategoryId  BIGINT=NULL,
@HomePageActive BIT = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Title]
	 	,
	 		R.[Description]
	 	,
	 		R.[CategoryId],
			category.[Name],
			R.Slug,
			R.VideoPath,
			R.HomePageActive

,
	 		FileXml=(
			  SELECT 
			   FG.Id,
			   FG.CreatedBy,
			   FG.UpdatedBy,
			   FG.CreatedOn,
			   FG.UpdatedOn,
			   FG.IsDeleted,
			   FG.IsActive,
			   FG.Filename,
			   FG.MimeType,
			   FG.Thumbnail,
			   FG.Size,
			   FG.Path,
			   FG.OriginalName,
			   FG.OnServer,
			   FG.TypeId,
			   FG.Type
			   FROM FileGroupItems FG
			   WHERE FG.TypeId = R.Id AND FG.Type=@FileType AND FG.IsActive = 1 AND FG.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	overall_count = COUNT(*) OVER()
	FROM [Blog] R  
				JOIN Category category ON category.Id=R.CategoryId 
	 		
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		@HomePageActive IS NULL
			OR
		R.HomePageActive = @HomePageActive
	)
	AND
	(
		R.IsDeleted = 0
	)
	AND
	(
		@Slug IS NULL
			OR
		R.Slug =  @Slug
	)
	AND
	(
		@CategoryId IS NULL	
			OR
		R.CategoryId =  @CategoryId
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END