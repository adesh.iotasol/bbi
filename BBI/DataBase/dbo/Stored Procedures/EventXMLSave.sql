﻿CREATE PROCEDURE [dbo].[EventXMLSave]
 @EventXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Title)[1]', 'NVARCHAR') AS 'Title',
		NDS.DT.value('(Description)[1]', 'NVARCHAR') AS 'Description',
		NDS.DT.value('(StartDate)[1]', 'DATETIMEOFFSET(7)') AS 'StartDate',
		NDS.DT.value('(EndDate)[1]', 'DATETIMEOFFSET(7)') AS 'EndDate',
		NDS.DT.value('(EventbriteLink)[1]', 'NVARCHAR') AS 'EventbriteLink',
		NDS.DT.value('(VideoPath)[1]', 'NVARCHAR') AS 'VideoPath',
		NDS.DT.value('(Address)[1]','NVARCHAR') AS 'Address',
		NDS.DT.value('(EventTime)[1]','NVARCHAR') AS 'EventTime'
  FROM 
	@EventXml.nodes('/root/Event') AS NDS(DT)
   )
   MERGE INTO Event R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.CreatedBy =ISNULL(x.CreatedBy ,R.CreatedBy),R.UpdatedBy =ISNULL(x.UpdatedBy ,R.UpdatedBy),R.CreatedOn =ISNULL(x.CreatedOn ,R.CreatedOn),R.UpdatedOn =ISNULL(x.UpdatedOn ,R.UpdatedOn),R.IsDeleted =ISNULL(x.IsDeleted ,R.IsDeleted),R.IsActive =ISNULL(x.IsActive ,R.IsActive),R.Title =ISNULL(x.Title ,R.Title),R.Description =ISNULL(x.Description ,R.Description),R.StartDate =ISNULL(x.StartDate ,R.StartDate),R.EndDate =ISNULL(x.EndDate ,R.EndDate),R.EventbriteLink = ISNULL(x.EventbriteLink, R.EventbriteLink), R.VideoPath = ISNULL(x.VideoPath, R.VideoPath),R.[Address] = ISNULL(x.[Address], R.[Address]),R.EventTime = ISNULL(x.EventTime, R.EventTime)
  WHEN NOT MATCHED 
  THEN 
    INSERT
    VALUES(
    x.CreatedBy,x.UpdatedBy,x.CreatedOn,x.UpdatedOn,x.IsDeleted,x.IsActive,x.Title,x.Description,x.StartDate,x.EndDate,x.EventbriteLink, x.VideoPath, x.[Address], x.[EventTime]
    );
END