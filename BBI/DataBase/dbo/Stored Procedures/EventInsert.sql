﻿
CREATE PROCEDURE [dbo].[EventInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@StartDate  DATETIMEOFFSET(7)=NULL,
		@EndDate  DATETIMEOFFSET(7)=NULL,
		@EventbriteLink NVARCHAR(MAX)= NULL,
		@VideoPath NVARCHAR(MAX)= NULL,
		@Address NVARCHAR(MAX)= NULL,
		@EventTime NVARCHAR(100) =NULL 
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Event]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[StartDate],[EndDate],[EventbriteLink],[VideoPath],[Address],[EventTime]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@StartDate,@EndDate,@EventbriteLink,@VideoPath,@Address,@EventTime
	  )
	SELECT SCOPE_IDENTITY()
 END