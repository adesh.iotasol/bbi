CREATE PROCEDURE [dbo].[AppointmentInsert](

	  	@CreatedBy			BIGINT=NULL,
		@AppointmentDate	DATETIMEOFFSET(7)=NULL,
		@Note				NVARCHAR(MAX)=NULL,
		@StartTime			TIME(7)=NULL,
		@EndTime			TIME(7)=NULL,
		@ToUserId			BIGINT=NULL,
		@FromUserId			BIGINT=NULL,
		@Status				NVARCHAR(50)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO Appointment
	  (
	   CreatedBy,
	   UpdatedBy,
	   AppointmentDate,
	   Note,
	   StartTime,
	   EndTime,
	   ToUserId,
	   FromUserId,
	   Status
	  )
	  VALUES
	  ( 
	   @CreatedBy,
	   @CreatedBy,
	   @AppointmentDate,
	   @Note,
	   @StartTime,
	   @EndTime,
	   @ToUserId,
	   @FromUserId,
	   @Status
	  )
	SELECT SCOPE_IDENTITY()
 END
 
 ---------------------------------------------------
 ---------------------------------------------------
 ﻿CREATE PROCEDURE [dbo].[AppointmentSelect](
@Id         BIGINT =NULL,
@ToUserId	BIGINT=NULL,
@FromUserId	BIGINT=NULL,
@AppointmentDate  DATETIMEOFFSET(7)=NULL,
@StartDate DATETIMEOFFSET(7)=NULL,
@EndDate DATETIMEOFFSET(7)=NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

SET NOCOUNT ON;
IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

;WITH AppointmentCTE AS(
	 SELECT 
	AP.Id,
	AP.CreatedBy,
	AP.UpdatedBy,
	AP.CreatedOn,
	AP.UpdatedOn,
	AP.IsDeleted,
	AP.IsActive,
	AP.AppointmentDate,
	AP.Note,
	AP.ToUserId,
	AP.FromUserId,
	AP.IsCancel,
	AP.CancellationReason,
	AP.StartTime,
	AP.EndTime,
	AP.isAppointmentDone,
	AP.Status,
	overall_count= COUNT(*) OVER()
	FROM Appointment AP
	WHERE  
	  (
	   @Id IS NULL
	   OR
	   AP.Id=@Id
	  ) 
	  AND
	  (
	   AP.IsDeleted=0
	  ) 
	  AND
	  (
	   AP.IsCancel=0
	  )
	  AND
	  (
		@FromUserId IS NULL
		OR
		AP.FromUserId=@FromUserId
	  )
	  AND
	  (
		@ToUserId IS NULL
		OR
		AP.ToUserId=@ToUserId
	  )
	  AND
	  (
		@AppointmentDate IS NULL
		OR
		CAST(AP.AppointmentDate as date) = CAST(@AppointmentDate as date)
	  )
	  AND
	  (
		  @StartDate IS NULL
		  OR
		  CONVERT(date, AP.AppointmentDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate) 
	  )
	 Order by AP.AppointmentDate desc
	 OFFSET (@next*@offset)-@next ROWS
	 FETCH NEXT @next ROWS ONLY
 )

 SELECT 
	CTE.Id,
	CTE.CreatedBy,
	CTE.UpdatedBy,
	CTE.CreatedOn,
	CTE.UpdatedOn,
	CTE.IsDeleted,
	CTE.IsActive,
	CTE.AppointmentDate,
	CTE.Note,
	CTE.ToUserId,
	CTE.FromUserId,
	CTE.IsCancel,
	CTE.CancellationReason,
	CTE.StartTime,
	CTE.EndTime,
	CTE.isAppointmentDone,
	CTE.Status,
	CTE.overall_count,
	US.FirstName AS FromUserFirstName,
	US.LastName AS FromUserLastName,
	US.Email AS FromUserEmail,
	US.PhoneNumber AS FromUserPhoneNumber,
	TU.FirstName AS ToUserFirstName,
	TU.LastName AS ToUserLastName,
	TU.Email AS ToUserEmail,
	TU.PhoneNumber AS ToUserPhoneNumber,
	UP.FirstName AS UpdatedByFirstName,
	UP.LastName AS UpdatedByLastName,
	UP.PhoneNumber AS UpdatedByPhoneNumber,
	UC.FirstName AS CreatedByFirstName,
	UC.LastName AS  CreatedByLastName,
	UC.PhoneNumber AS  CreatedByPhoneNumber 
 FROM AppointmentCTE CTE
	LEFT JOIN Users US ON CTE.FromUserId=US.Id
	LEFT JOIN Users TU ON CTE.ToUserId=TU.Id
	LEFT JOIN Users UP ON CTE.UpdatedBy = UP.Id
	LEFT JOIN Users UC ON CTE.CreatedBy = UC.Id


END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AppointmentUpdate]
	  	@Id						BIGINT=NULL,
	  	@UpdatedBy				BIGINT=NULL,
		@UpdatedOn				DATETIMEOFFSET(7)=NULL,
	  	@IsActive				BIT=NULL,
		@AppointmentDate		DATETIMEOFFSET(7)=NULL,
		@Note					NVARCHAR(MAX)=NULL,
	  	@ToUserId				BIGINT=NULL,
		@FromUserId				BIGINT=NULL,
		@IsCancel				BIT=NULL,
		@CancellationReason		NVARCHAR(MAX)=NULL,
		@StartTime				TIME(7)=NULL,
		@EndTime				TIME(7)=NULL,
		@isAppointmentDone		BIT=NULL,
		@Status					NVARCHAR(50)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Appointment
	SET
	 UpdatedBy			= ISNULL(@UpdatedBy,UpdatedBy),	 		
	 UpdatedOn			= switchoffset(sysdatetimeoffset(),'+00:00'),		
	 IsActive			= ISNULL(@IsActive,IsActive),	 		
	 AppointmentDate	= ISNULL(@AppointmentDate,AppointmentDate),	
	 Note				= ISNULL(@Note,Note),	 		
	 ToUserId			= ISNULL(@ToUserId,ToUserId),
	 FromUserId			= ISNULL(@FromUserId,FromUserId),
	 IsCancel			= ISNULL(@IsCancel,IsCancel),
	 CancellationReason = ISNULL(@CancellationReason,CancellationReason),
	 StartTime			= ISNULL(@StartTime,StartTime),
	 EndTime			= ISNULL(@EndTime,EndTime),
	 isAppointmentDone	= ISNULL(@isAppointmentDone,isAppointmentDone),
	 Status				= ISNULL(@Status,Status)	 		
	 WHERE
	 (
	  Id=@Id
	 )
END
---------------------------------------------------
---------------------------------------------------
﻿
CREATE PROCEDURE [dbo].[DailyWorkSettingAction]
(	
	@Sun				BIT=NULL,
	@Mon				BIT=NULL,
	@Tue				BIT=NULL,
	@Wed				BIT=NULL,
	@Thu				BIT=NULL,
	@Fri				BIT=NULL,
	@Sat				BIT=NULL,
	@StartTime			TIME(7)=NULL,
	@EndTime			TIME(7)=NULL,
	@StartLunchTime		TIME(7)=NULL,
	@EndLunchTime		TIME(7)=NULL,
	@CreatedBy			BIGINT=NULL,
	@Id					INT=NULL
	
)
AS
BEGIN
	SET NOCOUNT OFF;
		BEGIN
		IF(@Id IS NULL)
		BEGIN
			INSERT INTO [dbo].[DailyWorkSetting]
			(			
				[Sunday],
				[Monday],
				[Tuesday],
				[Wednesday],
				[Thursday],
				[Friday],
				[Satureday],
				[StartTime],
				[EndTime],
				[StartLunchTime],
				[EndLunchTime],
				[CreatedBy],
				UpdatedBy
			)
			VALUES 
			(			
				@Sun,
				@Mon,
				@Tue,
				@Wed,
				@Thu,
				@Fri,
				@Sat, 
				@StartTime,  
				@EndTime, 
				@StartLunchTime,
				@EndLunchTime,
				@CreatedBy,
				@CreatedBy
			);
			SELECT CAST (SCOPE_IDENTITY() AS bigint)  AS Id;
		END
		ELSE
		BEGIN
			UPDATE 
				[dbo].[DailyWorkSetting]
			SET 
				[Sunday]			= ISNULL(@Sun, [Sunday]),
				[Monday]			= ISNULL(@Mon, [Monday]),
				[Tuesday]			= ISNULL(@Tue, [Tuesday]),
				[Wednesday]			= ISNULL(@Wed, [Wednesday]),
				[Thursday]			= ISNULL(@Thu, [Thursday]),
				[Friday]			= ISNULL(@Fri, [Friday]),
				[Satureday]			= ISNULL(@Sat, [Satureday]),
				[StartTime]			= ISNULL(@StartTime, [StartTime]),
				[EndTime]			= ISNULL(@EndTime, [EndTime]),
				[StartLunchTime]	= ISNULL(@StartLunchTime, [StartLunchTime]),
				[EndLunchTime]		= ISNULL(@EndLunchTime, [EndLunchTime]),
				[UpdatedDate]		= SYSDATETIMEOFFSET(),
				[UpdatedBy]			= ISNULL(@CreatedBy, UpdatedBy)
			WHERE
			(
				([Id] = @Id)
			)
			SELECT @Id
		END
		END
	End
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[DailyWorkSettingSelect]

AS
BEGIN
	SET NOCOUNT OFF;
		BEGIN
			SELECT * FROM DailyWorkSetting
		END
End
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DummyTableForFileSave]
	@Name			NVARCHAR(100)=NULL,
	@CreatedBy		BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO DummyTableForFile
	(
		Name,
		CreatedBy,
		UpdatedBy
	)
	VALUES
	(
		@Name,
		@CreatedBy,
		@CreatedBy
	)
	SELECT SCOPE_IDENTITY()
END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DummyTableForFileSelect]
	@Id		BIGINT=NULL,
	@Type	NVARCHAR(50)=NULL,
	@next int = NULL,
	@offset int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @next IS NULL
	BEGIN
		SET @next =100000
		SET @offset=1
	END

	SELECT 
	D.Id,
	D.Name,
	D.IsActive,
	D.CreatedBy,
	D.UpdatedBy,
	FileGroupItemsXml=(
		SELECT 
			FG.Id,
			FG.CreatedBy,
			FG.UpdatedBy,
			FG.CreatedOn,
			FG.UpdatedOn,
			FG.IsDeleted,
			FG.IsActive,
			FG.Filename,
			FG.MimeType,
			FG.Thumbnail,
			FG.Size,
			FG.Path,
			FG.OriginalName,
			FG.OnServer,
			FG.TypeId,
			FG.Type
			FROM FileGroupItems FG
			WHERE FG.TypeId = D.Id AND FG.Type=@Type AND FG.IsActive = 1 AND FG.IsDeleted = 0
			FOR XML AUTO,ROOT,ELEMENTs
	),
	UP.FirstName AS UpdatedByFirstName,
	UP.LastName AS UpdatedByLastName,
	UP.PhoneNumber AS UpdatedByPhoneNumber,
	UC.FirstName AS CreatedByFirstName,
	UC.LastName AS  CreatedByLastName,
	UC.PhoneNumber AS  CreatedByPhoneNumber
	FROM 
	DummyTableForFile D
	LEFT JOIN Users UP ON D.UpdatedBy = UP.Id
	LEFT JOIN Users UC ON D.CreatedBy = UC.Id
	WHERE
	(
		@Id IS NULL
		OR
		D.Id = @Id
	)
	Order by D.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY
END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DummyTableForFileUpdate]
	@Name			NVARCHAR(100)=NULL,
	@UpdatedBy		BIGINT=NULL,
	@IsDeleted		BIT=NULL,
	@IsActive		BIT=NULL,
	@Id				BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE DummyTableForFile SET
	Name=ISNULL(@Name, Name),
	IsDeleted=ISNULL(@IsDeleted, IsDeleted),
	IsActive=ISNULL(@IsActive, IsActive),
	UpdatedBy=ISNULL(@UpdatedBy, UpdatedBy),
	UpdatedOn=switchoffset(sysdatetimeoffset(),'+00:00')
	WHERE 
	Id=@Id
END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EmailConfigurationSelect]
	@ConfigurationKey NVARCHAR(200)=NULL
AS
BEGIN
	SELECT * FROM EmailConfiguration WHERE ConfigurationKey=@ConfigurationKey AND IsDeleted=0
END

---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[FileGroupInsert]
(
	  	@CreatedBy    BIGINT=NULL,
		@Name		NVARCHAR(250)=NULL,
		@Type		NVARCHAR(50)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [FileGroup]
	  (
	   CreatedBy,UpdatedBy,Name, [Type]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@CreatedBy,@Name, @Type
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.FileGroupItemInsert
	@CreatedBy		BIGINT=NULL,
	@FileName		NVARCHAR(250)=NULL,
	@MimeType		NVARCHAR(250)=NULL,
	@Thumbnail		NVARCHAR(250)=NULL,
	@Size			BIGINT=NULL,
	@Path			NVARCHAR(250)=NULL,
	@OriginalName	NVARCHAR(250)=NULL,
	@OnServer		NVARCHAR(250)=NULL,
	@TypeId			BIGINT=NULL,
	@Type			NVARCHAR(50)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO FileGroupItems
	(
		CreatedBy,
		UpdatedBy,		
		FileName,		
		MimeType,		
		Thumbnail,		
		Size,			
		Path,			
		OriginalName,	
		OnServer,		
		TypeId,			
		Type			
	)
	VALUES
	(
		@CreatedBy,
		@CreatedBy,		
		@FileName,		
		@MimeType,		
		@Thumbnail,	
		@Size,			
		@Path,			
		@OriginalName,	
		@OnServer,		
		@TypeId,			
		@Type			
	)
	SELECT SCOPE_IDENTITY()
END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FileGroupItemsDelete]
	@Id			BIGINT=NULL,
	@UpdatedBy	BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
   UPDATE FileGroupItems 
   SET 
   IsDeleted=1,
   UpdatedBy=@UpdatedBy,
   UpdatedOn=switchoffset(sysdatetimeoffset(),'+00:00')
   WHERE  
   Id=@Id
END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[FileGroupItemsInsertXml](
	@UserId			BIGINT = NULL,
	@TypeId		BIGINT=NULL,
	@FileGroupItemsXml XML = NULL
)
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @FileGroupItems XML = NULL;
	SET @FileGroupItems = @FileGroupItemsXml;
	;WITH XmlData AS 
	(
		SELECT
			FileGroupItemsXml.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
			FileGroupItemsXml.DT.value('(Filename)[1]', 'nvarchar(256)') AS 'Filename',
			FileGroupItemsXml.DT.value('(MimeType)[1]', 'nvarchar(256)') AS 'MimeType',
			FileGroupItemsXml.DT.value('(Thumbnail)[1]', 'nvarchar(256)') AS 'Thumbnail',
			FileGroupItemsXml.DT.value('(Size)[1]', 'BIGINT') AS 'Size',
			FileGroupItemsXml.DT.value('(Path)[1]', 'nvarchar(256)') AS 'Path',
			FileGroupItemsXml.DT.value('(OriginalName)[1]', 'nvarchar(256)') AS 'OriginalName',
			FileGroupItemsXml.DT.value('(OnServer)[1]', 'nvarchar(256)') AS 'OnServer',
			FileGroupItemsXml.DT.value('(Type)[1]', 'nvarchar(50)') AS 'Type'
		FROM 
        @FileGroupItems.nodes('/ArrayOfFileGroupItemsModel/FileGroupItemsModel') AS FileGroupItemsXml(DT)
	 )
	 MERGE INTO FileGroupItems AF
	 USING XmlData x on AF.Id=x.Id
	 WHEN MATCHED 
		THEN UPDATE SET
			AF.UpdatedBy = @UserId,
			AF.Filename = x.Filename,
			AF.MimeType = x.MimeType,
			AF.Thumbnail = x.Thumbnail,
			AF.Size = x.Size,
			AF.Path = x.Path,
			AF.OriginalName = x.OriginalName,
			AF.OnServer = x.OnServer,
			AF.TypeId = @TypeId,
			AF.Type=x.Type
		
	WHEN NOT MATCHED 
	THEN 
	
      INSERT
	  (
	   CreatedBy,UpdatedBy,Filename,MimeType,Thumbnail,Size,Path,OriginalName,OnServer,TypeId, [Type]
	  )
      VALUES
	  ( 
	   @UserId,@UserId,x.Filename,x.MimeType,x.Thumbnail,x.Size,x.Path,x.OriginalName,x.OnServer,@TypeId, x.Type 
	  );
END
---------------------------------------------------
---------------------------------------------------
﻿/****** created by = kunal gupta ******/
/****** created date = 06-06-2017 ******/

CREATE PROCEDURE InsertExceptionLog(
	@Source        NVARCHAR(1000)=NULL,
	@Message       Nvarchar(1000)=NULL,
	@StackTrace    NVARCHAR(MAX)=NULL,
	@Uri           NVARCHAR(1000)=NULL,
	@method        NVARCHAR(50)=NULL,
	@CreatedBy     BIGINT=NULL
)
AS BEGIN
	INSERT INTO ExceptionLog
	(
		Source,
		Message,
		StackTrace,
		Uri,
		method,
		CreatedBy

	)
	Values
	(
		@Source,
		@Message,
		@StackTrace,
		@Uri,
		@method,
		@CreatedBy
	)
	SELECT SCOPE_IDENTITY()
END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[LeaveInsert](

	  	@CreatedBy		BIGINT				=NULL,
		@StartDate		DATETIMEOFFSET(7)	=NULL,
		@EndDate		DATETIMEOFFSET(7)	=NULL,
		@StartTime		TIME(7)				=NULL,
		@EndTime		TIME(7)				=NULL,
		@Type			NVARCHAR(50)		=NULL,
		@Description	NVARCHAR(MAX)		=NULL,
		@UserId			BIGINT					=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Leave]
	  (
	   [CreatedBy],[UpdatedBy],[StartDate],[EndDate],[StartTime], [EndTime], [Type], [Description],[UserId]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@CreatedBy,@StartDate,@EndDate, @StartTime, @EndTime, @Type, @Description,@UserId
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[LeaveSelect] (
@Id	        BIGINT =NULL,
@userId BIGINT = NULL,
@AppointmentDate DATETIMEOFFSET(7)=NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
 	R.[Id],
	R.[CreatedBy],
	R.[UpdatedBy],
	R.[CreatedOn],
	R.[UpdatedOn],
	R.[IsDeleted],
	R.[IsActive],
	R.[StartDate],
	R.[EndDate],
	R.[StartTime],
	R.[EndTime],
	R.[Type],
	R.[Description],
	R.[UserId],
	U.FirstName,
	U.LastName,
	FullName=U.FirstName+' '+U.LastName,
	overall_count = COUNT(*) OVER()
	FROM [Leave] R  INNER JOIN Users U ON R.UserId=U.Id
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@userId IS NULL
		OR
		R.UserId = @userId
	)
	AND
	(
		R.IsDeleted = 0
	)
	AND
	(
		@AppointmentDate IS NULL
		OR
		CONVERT(date, @AppointmentDate) between CONVERT(date, R.StartDate) AND CONVERT(date, R.EndDate)
	)
	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LeaveUpdate]
	  	@Id				BIGINT=NULL,
	  	@UpdatedBy		BIGINT=NULL,
		@UpdatedOn		DATETIMEOFFSET(7)=NULL,
	  	@IsDeleted		BIT=NULL,
	  	@IsActive		BIT=NULL,
		@StartDate		DATETIMEOFFSET(7)=NULL,
		@EndDate		DATETIMEOFFSET(7)=NULL,
		@StartTime		Time(7)=NULL,
		@EndTime		Time(7)=NULL,
		@Type			NVARCHAR(20)=NULL,
		@Description	NVARCHAR(MAX)=NULL,
		@UserId			BIGINT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Leave]
	SET
	 [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),	 		
	 [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
	 [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),	 		
	 [IsActive] = ISNULL(@IsActive,[IsActive]),	 		
	 [StartDate] = ISNULL(@StartDate,[StartDate]),	 		
	 [EndDate] = ISNULL(@EndDate,[EndDate]),	 		
	 [StartTime] = ISNULL(@StartTime,[StartTime]),	 		
	 [EndTime] = ISNULL(@EndTime,[EndTime]),	 		
	 [Type] = ISNULL(@Type,[Type]),	 		
	 [Description] = ISNULL(@Description,[Description]),	 		
	 [UserId] = ISNULL(@UserId,[UserId])	 		
	 WHERE
	 (
	  Id=@Id
	 )
END
---------------------------------------------------
---------------------------------------------------

﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PublicHolidaysInsert]
(	
	@StartHoliday	DATETIMEOFFSET(7),
	@EndHoliday	DATETIMEOFFSET(7),
	@Description	NVARCHAR(500) = Null,
	@CreatedBy		BIGINT=NULL
)
AS
BEGIN
	SET NOCOUNT OFF;
		INSERT INTO [dbo].[PublicHolidays]
		(			
			StartHoliday,
			EndHoliday,
			[Description],
			[CreatedBy],
			[UpdatedBy]
		)
		VALUES 
		(			
			@StartHoliday,
			@EndHoliday, 
			@Description, 
			@CreatedBy,
			@CreatedBy
		);
		SELECT SCOPE_IDENTITY()
	End
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PublicHolidaysSelect]
(	
	@Id				BIGINT=NULL,
	@TodayDate		DATETIMEOFFSET(7)=NULL,
	@StartDate		DATETIMEOFFSET(7)=NULL,
	@EndDate		DATETIMEOFFSET(7)=NULL
)
AS
BEGIN
	SET NOCOUNT OFF;
		SELECT 
		P.Id,
		P.StartHoliday,
		P.EndHoliday,
		P.Description,
		P.IsDeleted,
		P.CreatedBy,
		P.UpdatedBy,
		P.CreatedDate,
		P.UpdatedDate,
		IsOnlyView=(CASE WHEN P.StartHoliday <= @TodayDate THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END)
		FROM PublicHolidays P 
		WHERE
		(
			@Id IS NULL
			OR
			P.Id=@Id
		)
		AND
		(
			@StartDate IS NULL
			OR
			CONVERT(date, P.StartHoliday) BETWEEN CONVERT(date, @StartDate)  AND CONVERT(date, @EndDate)
		)
		AND P.IsDeleted=0
		ORDER BY P.StartHoliday DESC
	End
---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PublicHolidaysUpdate]
(	
	@StartHoliday	DATETIMEOFFSET(7),
	@EndHoliday		DATETIMEOFFSET(7),
	@Description	NVARCHAR(500) = Null,
	@UpdatedBy		BIGINT=NULL,
	@IsDeleted		BIT=NULL,
	@Id				BIGINT=NULL
	
)
AS
BEGIN
	SET NOCOUNT OFF;
		UPDATE PublicHolidays SET
		StartHoliday=ISNULL(@StartHoliday, StartHoliday),
		EndHoliday=ISNULL(@EndHoliday,EndHoliday),
		Description=ISNULL(@Description, Description),
		UpdatedBy=ISNULL(@UpdatedBy, UpdatedBy),
		UpdatedDate=(switchoffset(sysdatetimeoffset(),'+00:00')),
		IsDeleted=ISNULL(@IsDeleted, IsDeleted)
		WHERE 
		Id=@Id
End
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[SelectUserProfileByUniqueCode]
	@UniqueCode NVARCHAR(65)=NULL
AS
BEGIN
	
	SELECT 
	U.Email,
	U.Id,
	U.IsActive,
	U.PhoneNumber,
	U.UserName,
	U.firstName,
	U.lastName,
	U.ProfilePic,
	U.UniqueCode
	FROM Users U
	WHERE 
	U.IsDeleted=0
	AND
	(
		@UniqueCode IS NULL
		OR
		U.UniqueCode=@UniqueCode
	)
END
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[UpdateUserProfile](
	@UserId          BIGINT = NULL,
	@FirstName       NVARCHAR(100) = NULL,
	@LastName        NVARCHAR(100) = NULL,
	@PhoneNumber     NVARCHAR(30) = NULL,
	@IsActive        BIT = NULL,
	@ProfileImageUrl	NVARCHAR(MAX)=NULL
)
AS
BEGIN
	UPDATE Users SET
	firstName       =     ISNULL(@FirstName,firstName),
	lastName        =     ISNULL(@LastName,lastName),
	PhoneNumber     =     ISNULL(@PhoneNumber,PhoneNumber),
	IsActive       =      ISNULL(@IsActive,IsActive),
	ProfilePic		=		ISNULL(@ProfileImageUrl, ProfilePic) 
WHERE

Id = @UserId

END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[UploadedFileInsert]
(
@FileName NVARCHAR(250),
@FileUrl NVARCHAR(max)

)
AS BEGIN
SET NOCOUNT ON;
INSERT INTO UploadedFile

(
	FileName,
	FileUrl,
	IsDeleted,
	CreatedOn

)
VALUES
(
	@FileName,
	@FileUrl,
	0,
	switchoffset(sysdatetimeoffset(),'+00:00')

)

SELECT @@IDENTITY
END
---------------------------------------------------
---------------------------------------------------
CREATE TABLE [dbo].[Category] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Category_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Category_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Category_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Category_IsActive] DEFAULT ((1)) NOT NULL,
		[Name] NVARCHAR (255) NOT NULL,
CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([Id]))
---------------------------------------------------
---------------------------------------------------
﻿
CREATE PROCEDURE [dbo].[CategoryInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Name NVARCHAR(MAX)=NULL
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Category]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Name]
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Name
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[CategorySelect](
@Id	        BIGINT =NULL,
@IsActive bit = NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Name]
	 	,
	overall_count = COUNT(*) OVER()
	FROM [Category] R  
	 		
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		R.IsDeleted = 0
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END

---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CategoryUpdate
	  	@Id    BIGINT=NULL,
	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
		@CreatedOn  DATETIMEOFFSET(7)=NULL,
		@UpdatedOn  DATETIMEOFFSET(7)=NULL,
	  	@IsDeleted    BIT=NULL,
	  	@IsActive    BIT=NULL,
		@Name NVARCHAR(MAX)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Category]
	SET
		 [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
	 [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
		 [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
		 [IsActive] = ISNULL(@IsActive,[IsActive]),
		 [Name] = ISNULL(@Name,[Name])
	 WHERE
	 (
	  Id=@Id
	 )
END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[CategoryXMLSave]
 @CategoryXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Name)[1]', 'NVARCHAR') AS 'Name'
  FROM 
	@CategoryXml.nodes('/root/Category') AS NDS(DT)
   )
   MERGE INTO Category R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.CreatedBy =ISNULL(x.CreatedBy ,R.CreatedBy),R.UpdatedBy =ISNULL(x.UpdatedBy ,R.UpdatedBy),R.CreatedOn =ISNULL(x.CreatedOn ,R.CreatedOn),R.UpdatedOn =ISNULL(x.UpdatedOn ,R.UpdatedOn),R.IsDeleted =ISNULL(x.IsDeleted ,R.IsDeleted),R.IsActive =ISNULL(x.IsActive ,R.IsActive),R.Name =ISNULL(x.Name ,R.Name)
  WHEN NOT MATCHED 
  THEN 
    INSERT
    VALUES(
    x.CreatedBy,x.UpdatedBy,x.CreatedOn,x.UpdatedOn,x.IsDeleted,x.IsActive,x.Name
    );
END
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
CREATE TABLE [dbo].[Blog] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Blog_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Blog_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Blog_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Blog_IsActive] DEFAULT ((1)) NOT NULL,
		[Title] NVARCHAR (255) NOT NULL,
		[Description] NVARCHAR (MAX) NULL,
	    [CategoryId] BIGINT NOT NULL,
CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED ([Id]))
---------------------------------------------------
---------------------------------------------------
﻿
CREATE PROCEDURE [dbo].[BlogInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
	    @CategoryId BIGINT=NULL,
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Blog]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[CategoryId],
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@CategoryId,
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[BlogSelect](
@Id	        BIGINT =NULL,
@FileType NVARCHAR(50),
@IsActive bit = NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Title]
	 	,
	 		R.[Description]
	 	,
	 		R.[CategoryId],
				category.[Name]
,
	 		FileXml=(
			  SELECT 
			   FG.Id,
			   FG.CreatedBy,
			   FG.UpdatedBy,
			   FG.CreatedOn,
			   FG.UpdatedOn,
			   FG.IsDeleted,
			   FG.IsActive,
			   FG.Filename,
			   FG.MimeType,
			   FG.Thumbnail,
			   FG.Size,
			   FG.Path,
			   FG.OriginalName,
			   FG.OnServer,
			   FG.TypeId,
			   FG.Type
			   FROM FileGroupItems FG
			   WHERE FG.TypeId = R.Id AND FG.Type=@FileType AND FG.IsActive = 1 AND FG.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	overall_count = COUNT(*) OVER()
	FROM [Blog] R  
				JOIN Category category ON category.Id=R.CategoryId 
	 		
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		R.IsDeleted = 0
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END

---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE BlogUpdate
	  	@Id    BIGINT=NULL,
	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
		@CreatedOn  DATETIMEOFFSET(7)=NULL,
		@UpdatedOn  DATETIMEOFFSET(7)=NULL,
	  	@IsDeleted    BIT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@CategoryId   BIGINT=NULL,
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Blog]
	SET
		 [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
	 [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
		 [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
		 [IsActive] = ISNULL(@IsActive,[IsActive]),
		 [Title] = ISNULL(@Title,[Title]),
		 [Description] = ISNULL(@Description,[Description]),
		 [CategoryId] = ISNULL(@CategoryId,[CategoryId]),
	 WHERE
	 (
	  Id=@Id
	 )
END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[BlogXMLSave]
 @BlogXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Title)[1]', 'NVARCHAR') AS 'Title',
		NDS.DT.value('(Description)[1]', 'NVARCHAR') AS 'Description',
		NDS.DT.value('(CategoryId)[1]', 'BIGINT') AS 'CategoryId',
  FROM 
	@BlogXml.nodes('/root/Blog') AS NDS(DT)
   )
   MERGE INTO Blog R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.CreatedBy =ISNULL(x.CreatedBy ,R.CreatedBy),R.UpdatedBy =ISNULL(x.UpdatedBy ,R.UpdatedBy),R.CreatedOn =ISNULL(x.CreatedOn ,R.CreatedOn),R.UpdatedOn =ISNULL(x.UpdatedOn ,R.UpdatedOn),R.IsDeleted =ISNULL(x.IsDeleted ,R.IsDeleted),R.IsActive =ISNULL(x.IsActive ,R.IsActive),R.Title =ISNULL(x.Title ,R.Title),R.Description =ISNULL(x.Description ,R.Description),R.CategoryId =ISNULL(x.CategoryId ,R.CategoryId),
  WHEN NOT MATCHED 
  THEN 
    INSERT
    VALUES(
    x.CreatedBy,x.UpdatedBy,x.CreatedOn,x.UpdatedOn,x.IsDeleted,x.IsActive,x.Title,x.Description,x.CategoryId,
    );
END
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
CREATE TABLE [dbo].[Event] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Event_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Event_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Event_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Event_IsActive] DEFAULT ((1)) NOT NULL,
		[Title] NVARCHAR (255) NOT NULL,
		[Description] NVARCHAR (MAX) NULL,
		[StartDate]  DATETIMEOFFSET (7) CONSTRAINT [DF_Event_StartDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[EndDate]  DATETIMEOFFSET (7) CONSTRAINT [DF_Event_EndDate] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED ([Id]))
---------------------------------------------------
---------------------------------------------------
﻿
CREATE PROCEDURE [dbo].[EventInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@StartDate  DATETIMEOFFSET(7)=NULL,
		@EndDate  DATETIMEOFFSET(7)=NULL,
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Event]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[StartDate],[EndDate],
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@StartDate,@EndDate,
	  )
	SELECT SCOPE_IDENTITY()
 END
---------------------------------------------------
---------------------------------------------------
﻿CREATE PROCEDURE [dbo].[EventSelect](
@Id	        BIGINT =NULL,
@FileType NVARCHAR(50),
@IsActive bit = NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Title]
	 	,
	 		R.[Description]
	 	,
	 		R.[StartDate]
	 	,
	 		R.[EndDate]
	 	,
	 		FileXml=(
			  SELECT 
			   FG.Id,
			   FG.CreatedBy,
			   FG.UpdatedBy,
			   FG.CreatedOn,
			   FG.UpdatedOn,
			   FG.IsDeleted,
			   FG.IsActive,
			   FG.Filename,
			   FG.MimeType,
			   FG.Thumbnail,
			   FG.Size,
			   FG.Path,
			   FG.OriginalName,
			   FG.OnServer,
			   FG.TypeId,
			   FG.Type
			   FROM FileGroupItems FG
			   WHERE FG.TypeId = R.Id AND FG.Type=@FileType AND FG.IsActive = 1 AND FG.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	overall_count = COUNT(*) OVER()
	FROM [Event] R  
	 		
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		R.IsDeleted = 0
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END

---------------------------------------------------
---------------------------------------------------
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE EventUpdate
	  	@Id    BIGINT=NULL,
	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
		@CreatedOn  DATETIMEOFFSET(7)=NULL,
		@UpdatedOn  DATETIMEOFFSET(7)=NULL,
	  	@IsDeleted    BIT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@StartDate  DATETIMEOFFSET(7)=NULL,
		@EndDate  DATETIMEOFFSET(7)=NULL,
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Event]
	SET
		 [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
	 [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
		 [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
		 [IsActive] = ISNULL(@IsActive,[IsActive]),
		 [Title] = ISNULL(@Title,[Title]),
		 [Description] = ISNULL(@Description,[Description]),
		 [StartDate] = ISNULL(@StartDate,[StartDate]),
		 [EndDate] = ISNULL(@EndDate,[EndDate]),
	 WHERE
	 (
	  Id=@Id
	 )
END
---------------------------------------------------
---------------------------------------------------
CREATE PROCEDURE [dbo].[EventXMLSave]
 @EventXml xml
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 ;WITH XmlData AS 
 (
  SELECT
		NDS.DT.value('(Id)[1]', 'BIGINT') AS 'Id',
		NDS.DT.value('(CreatedBy)[1]', 'BIGINT') AS 'CreatedBy',
		NDS.DT.value('(UpdatedBy)[1]', 'BIGINT') AS 'UpdatedBy',
		NDS.DT.value('(CreatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'CreatedOn',
		NDS.DT.value('(UpdatedOn)[1]', 'DATETIMEOFFSET(7)') AS 'UpdatedOn',
	  	NDS.DT.value('(IsDeleted)[1]', 'BIT') AS 'IsDeleted',
	  	NDS.DT.value('(IsActive)[1]', 'BIT') AS 'IsActive',
		NDS.DT.value('(Title)[1]', 'NVARCHAR') AS 'Title',
		NDS.DT.value('(Description)[1]', 'NVARCHAR') AS 'Description',
		NDS.DT.value('(StartDate)[1]', 'DATETIMEOFFSET(7)') AS 'StartDate',
		NDS.DT.value('(EndDate)[1]', 'DATETIMEOFFSET(7)') AS 'EndDate',
  FROM 
	@EventXml.nodes('/root/Event') AS NDS(DT)
   )
   MERGE INTO Event R
   USING XmlData x on R.Id=x.Id
   WHEN MATCHED 
   THEN UPDATE SET
    R.CreatedBy =ISNULL(x.CreatedBy ,R.CreatedBy),R.UpdatedBy =ISNULL(x.UpdatedBy ,R.UpdatedBy),R.CreatedOn =ISNULL(x.CreatedOn ,R.CreatedOn),R.UpdatedOn =ISNULL(x.UpdatedOn ,R.UpdatedOn),R.IsDeleted =ISNULL(x.IsDeleted ,R.IsDeleted),R.IsActive =ISNULL(x.IsActive ,R.IsActive),R.Title =ISNULL(x.Title ,R.Title),R.Description =ISNULL(x.Description ,R.Description),R.StartDate =ISNULL(x.StartDate ,R.StartDate),R.EndDate =ISNULL(x.EndDate ,R.EndDate),
  WHEN NOT MATCHED 
  THEN 
    INSERT
    VALUES(
    x.CreatedBy,x.UpdatedBy,x.CreatedOn,x.UpdatedOn,x.IsDeleted,x.IsActive,x.Title,x.Description,x.StartDate,x.EndDate,
    );
END
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
---------------------------------------------------
--@@@@NEW_TABLE_SCRIPT@@@@



