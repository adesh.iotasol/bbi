﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE EventUpdate
	  	@Id    BIGINT=NULL,
	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
		@CreatedOn  DATETIMEOFFSET(7)=NULL,
		@UpdatedOn  DATETIMEOFFSET(7)=NULL,
	  	@IsDeleted    BIT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@StartDate  DATETIMEOFFSET(7)=NULL,
		@EndDate  DATETIMEOFFSET(7)=NULL,
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [Event]
	SET
		 [UpdatedBy] = ISNULL(@UpdatedBy,[UpdatedBy]),
	 [UpdatedOn] = switchoffset(sysdatetimeoffset(),'+00:00'),	
		 [IsDeleted] = ISNULL(@IsDeleted,[IsDeleted]),
		 [IsActive] = ISNULL(@IsActive,[IsActive]),
		 [Title] = ISNULL(@Title,[Title]),
		 [Description] = ISNULL(@Description,[Description]),
		 [StartDate] = ISNULL(@StartDate,[StartDate]),
		 [EndDate] = ISNULL(@EndDate,[EndDate]),
	 WHERE
	 (
	  Id=@Id
	 )
END