﻿CREATE PROCEDURE [dbo].[EventSelect](
@Id	        BIGINT =NULL,
@FileType NVARCHAR(50),
@IsActive bit = NULL,
@next int = NULL,
@offset int = NULL
)
AS BEGIN

IF @next IS NULL
BEGIN
SET @next =100000
SET @offset=1
END

 SELECT
	 		R.[Id]
	 	,
	 		R.[CreatedBy]
	 	,
	 		R.[UpdatedBy]
	 	,
	 		R.[CreatedOn]
	 	,
	 		R.[UpdatedOn]
	 	,
	 		R.[IsDeleted]
	 	,
	 		R.[IsActive]
	 	,
	 		R.[Title]
	 	,
	 		R.[Description]
	 	,
	 		R.[StartDate]
	 	,
	 		R.[EndDate]
	 	,
	 		FileXml=(
			  SELECT 
			   FG.Id,
			   FG.CreatedBy,
			   FG.UpdatedBy,
			   FG.CreatedOn,
			   FG.UpdatedOn,
			   FG.IsDeleted,
			   FG.IsActive,
			   FG.Filename,
			   FG.MimeType,
			   FG.Thumbnail,
			   FG.Size,
			   FG.Path,
			   FG.OriginalName,
			   FG.OnServer,
			   FG.TypeId,
			   FG.Type
			   FROM FileGroupItems FG
			   WHERE FG.TypeId = R.Id AND FG.Type=@FileType AND FG.IsActive = 1 AND FG.IsDeleted = 0
			   FOR XML AUTO,ROOT,ELEMENTs
			 )
,
	overall_count = COUNT(*) OVER()
	FROM [Event] R  
	 		
	WHERE 
	(
		@Id IS NULL
		OR
		R.Id = @Id
	)
	AND
	(
		@IsActive IS NULL
			OR
		R.IsActive = @IsActive
	)
	AND
	(
		R.IsDeleted = 0
	)

	Order by R.Id desc
	OFFSET (@next*@offset)-@next ROWS
    FETCH NEXT @next ROWS ONLY

END
