﻿
CREATE PROCEDURE [dbo].[BlogInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
	    @CategoryId BIGINT=NULL,
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Blog]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[CategoryId],
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@CategoryId,
	  )
	SELECT SCOPE_IDENTITY()
 END