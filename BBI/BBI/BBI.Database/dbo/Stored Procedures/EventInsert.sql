﻿
CREATE PROCEDURE [dbo].[EventInsert](

	  	@CreatedBy    BIGINT=NULL,
	  	@UpdatedBy    BIGINT=NULL,
	  	@IsActive    BIT=NULL,
		@Title NVARCHAR(MAX)=NULL,
		@Description NVARCHAR(MAX)=NULL,
		@StartDate  DATETIMEOFFSET(7)=NULL,
		@EndDate  DATETIMEOFFSET(7)=NULL,
)
AS 
BEGIN
SET NOCOUNT ON;
	  INSERT INTO [Event]
	  (
	   [CreatedBy],[UpdatedBy],[IsActive],[Title],[Description],[StartDate],[EndDate],
	  )
	  VALUES
	  ( 
	   @CreatedBy,@UpdatedBy,@IsActive,@Title,@Description,@StartDate,@EndDate,
	  )
	SELECT SCOPE_IDENTITY()
 END