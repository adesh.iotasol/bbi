CREATE TABLE [dbo].[Blog] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Blog_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Blog_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Blog_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Blog_IsActive] DEFAULT ((1)) NOT NULL,
		[Title] NVARCHAR (255) NOT NULL,
		[Description] NVARCHAR (MAX) NULL,
	    [CategoryId] BIGINT NOT NULL,
CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED ([Id]))