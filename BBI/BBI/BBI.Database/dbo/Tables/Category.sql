CREATE TABLE [dbo].[Category] (
		[Id]           BIGINT             IDENTITY (1, 1) NOT NULL,
	  	[CreatedBy]    BIGINT             NOT NULL,
	  	[UpdatedBy]    BIGINT             NOT NULL,
		[CreatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Category_CreatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
		[UpdatedOn]  DATETIMEOFFSET (7) CONSTRAINT [DF_Category_UpdatedOn] DEFAULT (switchoffset(sysdatetimeoffset(),'+00:00')) NULL,
	  	[IsDeleted]    BIT               CONSTRAINT [DF_Category_IsDeleted] DEFAULT ((0)) NOT NULL,
	  	[IsActive]    BIT               CONSTRAINT [DF_Category_IsActive] DEFAULT ((1)) NOT NULL,
		[Name] NVARCHAR (255) NOT NULL,
CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([Id]))