﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IotaSmartBuild.Common.Enums
{
    public enum ErrorCode
    {
    	    	 	Category=1000,
    	 	Blog=2000,
    	 	Event=3000,
    }
    public enum ErrorFunctionCode
    {	
    	 	Category_Select=1001,
    	 	Category_Single_Select=1002,
    	 	Category_Insert=1003,
    	 	Category_Update=1004,
    	 	Category_Delete=1005,
    	 	Blog_Select=2001,
    	 	Blog_Single_Select=2002,
    	 	Blog_Insert=2003,
    	 	Blog_Update=2004,
    	 	Blog_Delete=2005,
    	 	Event_Select=3001,
    	 	Event_Single_Select=3002,
    	 	Event_Insert=3003,
    	 	Event_Update=3004,
    	 	Event_Delete=3005,
    }
}
