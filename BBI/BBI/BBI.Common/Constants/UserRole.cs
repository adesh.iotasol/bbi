﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Common.Constants
{
    public class UserRole
    {
        public const string Admin = "ROLE_ADMIN";
        public const string User = "ROLE_USER";
    }
}
