﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain.Upload;

namespace BBI.Domain.Blog
{
    public class BlogModel
    {
        public long? Id { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTimeOffset? CreatedOn { get; set; }
        public DateTimeOffset? UpdatedOn { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsActive { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public long? CategoryId { get; set; }
        public string FileXml { get; set; }
        public FileGroupItemsModel File { get; set; }
        public int TotalCount { get; set; }
        public string Slug { get; set; }
        public string VideoPath { get; set; }
        public bool? ShowOnHomePage { get; set; }
    }
}
