﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BBI.Core.Infrastructure;
using BBI.Service.Upload;
using BBI.Service.Users;
using BBI.Service.Xml;
using BBI.Service.FileGroup;
using BBI.Service.Category;
using BBI.Service.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Service.Configuration;
using BBI.Service.Exception;
using BBI.Service.Event;
using BBI.Service.Contact;

namespace BBI.Framework
{
    public class DependancyRegisterar : IDependencyRegistrar
    {
        public int Order
        {
            get
            {
                return 0;
            }
        }

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            var assemblyArr = typeFinder.GetAssemblies().ToArray();
            builder.RegisterControllers(assemblyArr);
            builder.RegisterApiControllers(assemblyArr);

            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<EmailConfigurationService>().As<IEmailConfigurationService>().InstancePerLifetimeScope();
            builder.RegisterType<FileUploadService>().As<IFileUploadService>().InstancePerLifetimeScope();
            builder.RegisterType<XmlService>().As<IXmlService>().InstancePerLifetimeScope();
            builder.RegisterType<FileGroupService>().As<IFileGroupService>().InstancePerLifetimeScope();
            builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerLifetimeScope();
            builder.RegisterType<BlogService>().As<IBlogService>().InstancePerLifetimeScope();
            builder.RegisterType<ExceptionService>().As<IExceptionService>().InstancePerLifetimeScope();
            builder.RegisterType<EventService>().As<IEventService>().InstancePerLifetimeScope();
            builder.RegisterType<ContactService>().As<IContactService>().InstancePerLifetimeScope();

        }
    }
}
