﻿using BBI.Domain.Category;
using BBI.Framework.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Framework.WebExtensions
{
    public static class CategoryModelViewModelConverter
    {
        public static CategoryViewModel ToViewModel(this CategoryModel x)
        {
            if (x == null) return new CategoryViewModel();
            return new CategoryViewModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
				TotalCount=x.TotalCount,
                BlogCount = x.BlogCount
            };
        }
		
		 public static CategoryModel ToModel(this CategoryViewModel x)
        {
            if (x == null) return new CategoryModel();
            return new CategoryModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Name = x.Name,
                    BlogCount = x.BlogCount
            };
        }
    }
}
