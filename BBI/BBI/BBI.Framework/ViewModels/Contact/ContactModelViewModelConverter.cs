﻿using BBI.Domain.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Framework.ViewModels.Contact
{
    public static class ContactModelViewModelConverter
    {
        public static ContactViewModel ToViewModel(this ContactModel x)
        {
            if (x == null) return new ContactViewModel();
            return new ContactViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Email,
                Subject  = x.Subject,
                Message = x.Message,
                CreatedOn  = x.CreatedOn
            };
        }

        public static ContactModel ToModel(this ContactViewModel x)
        {
            if (x == null) return new ContactModel();
            return new ContactModel
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Email,
                Subject = x.Subject,
                Message = x.Message,
            };
        }
    }
}
