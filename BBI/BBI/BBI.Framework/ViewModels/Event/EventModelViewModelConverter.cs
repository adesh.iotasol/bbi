﻿using BBI.Domain.Event;
using BBI.Framework.ViewModels.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Framework.WebExtensions
{
    public static class EventModelViewModelConverter
    {
        public static EventViewModel ToViewModel(this EventModel x)
        {
            if (x == null) return new EventViewModel();
            return new EventViewModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Title = x.Title,
        			Description = x.Description,
        			StartDate = x.StartDate,
        			EndDate = x.EndDate,
                    EventbriteLink = x.EventbriteLink,
        			File = x.File.ToViewModel(),
                    VideoPath = x.VideoPath,
                    EventTime = x.EventTime,
                    Address = x.Address,
				TotalCount=x.TotalCount
            };
        }
		
		 public static EventModel ToModel(this EventViewModel x)
        {
            if (x == null) return new EventModel();
            return new EventModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Title = x.Title,
        			Description = x.Description,
        			StartDate = x.StartDate,
        			EndDate = x.EndDate,
                    EventbriteLink = x.EventbriteLink,
        			File = x.File.ToModel(),
                    VideoPath = x.VideoPath,
                    EventTime = x.EventTime,
                    Address = x.Address
                    
            };
        }
    }
}
