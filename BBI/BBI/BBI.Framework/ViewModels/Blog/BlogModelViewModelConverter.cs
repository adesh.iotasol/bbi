﻿using BBI.Domain.Blog;
using BBI.Framework.ViewModels.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Framework.WebExtensions
{
    public static class BlogModelViewModelConverter
    {
        public static BlogViewModel ToViewModel(this BlogModel x)
        {
            if (x == null) return new BlogViewModel();
            return new BlogViewModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Title = x.Title,
        			Description = x.Description,
        			CategoryId = x.CategoryId,
                    CategoryName = x.CategoryName,
        			File = x.File.ToViewModel(),
				TotalCount=x.TotalCount,
                Slug = x.Slug,
                VideoPath = x.VideoPath,
                ShowOnHomePage = x.ShowOnHomePage
            };
        }
		
		 public static BlogModel ToModel(this BlogViewModel x)
        {
            if (x == null) return new BlogModel();
            return new BlogModel
            {
        			Id = x.Id,
        			CreatedBy = x.CreatedBy,
        			UpdatedBy = x.UpdatedBy,
        			CreatedOn = x.CreatedOn,
        			UpdatedOn = x.UpdatedOn,
        			IsDeleted = x.IsDeleted,
        			IsActive = x.IsActive,
        			Title = x.Title,
        			Description = x.Description,
        			CategoryId = x.CategoryId,
        			File = x.File.ToModel(),
                    Slug = x.Slug,
                    VideoPath = x.VideoPath,
                    ShowOnHomePage = x.ShowOnHomePage
            };
        }
    }
}
