﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Framework.ViewModels.Upload;

namespace BBI.Framework.ViewModels.Blog
{
    public class BlogViewModel
    {
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty("updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty("createdOn")]
        public DateTimeOffset? CreatedOn { get; set; }
        [JsonProperty("updatedOn")]
        public DateTimeOffset? UpdatedOn { get; set; }
        [JsonProperty("isDeleted")]
        public bool? IsDeleted { get; set; }
        [JsonProperty("isActive")]
        public bool? IsActive { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("categoryId")]
        public long? CategoryId { get; set; }
        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
        [JsonProperty("file")]
        public FileGroupItemsViewModel File { get; set; }
        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("videoPath")]
        public string VideoPath { get; set; }
        [JsonProperty("showOnHomePage")]
        public bool? ShowOnHomePage { get; set; }
    }
}

