﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BBI.Startup))]
namespace BBI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
