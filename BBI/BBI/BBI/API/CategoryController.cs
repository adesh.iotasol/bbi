using BBI.Framework.ViewModels.Category;
using BBI.Common.Constants;
using BBI.Common.Enums;
using BBI.Domain;
using BBI.Framework.CustomFilters;
using BBI.Framework.GenericResponse;
using BBI.Framework.WebExtensions;
using BBI.Service.Category;
using BBI.Service.Exception;
using BBI.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BBI.API
{
    [RoutePrefix("api")]
    [CustomExceptionFilter]
    public class CategoryController : ApiController
    {
        private ICategoryService _categoryService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Category
        /// </summary>
        /// <returns></returns>
        [Route("Categorys")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAllCategorys([FromUri] SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var categorys = _categoryService.SelectCategory(param).Select(x => x.ToViewModel()); ;
            	return Ok(categorys.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Category, (int)ErrorFunctionCode.Category_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Category_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("category/{id:long}")]
        [HttpGet]
        public IHttpActionResult GetCategory(long id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.id = id;
	            var category = _categoryService.SelectCategory(param).FirstOrDefault().ToViewModel();
	            return Ok(category.SuccessResponse());
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Category, (int)ErrorFunctionCode.Category_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Category_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("category")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult SaveCategory(CategoryViewModel model)
        {
        	try
            {
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _categoryService.CategoryInsert(model.ToModel());
	            return Ok(responseId.SuccessResponse("Category save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Category, (int)ErrorFunctionCode.Category_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Category_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("category")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpPut]
        public IHttpActionResult UpdateCategory(CategoryViewModel model)
        {
        	try{
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _categoryService.CategoryUpdate(model.ToModel());
	            return Ok("Category Update successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Category, (int)ErrorFunctionCode.Category_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Category_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("category/{id:long}")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpDelete]
        public IHttpActionResult DeleteCategory(long id)
        {
        	try{
	            CategoryViewModel model = new CategoryViewModel();
	            model.Id = id;
	            model.IsDeleted = true;
	            _categoryService.CategoryUpdate(model.ToModel());
	            return Ok("Category Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Category, (int)ErrorFunctionCode.Category_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Category_Delete).ToString().ErrorResponse());
            }
        }
    }
}
