using BBI.Framework.ViewModels.Blog;
using BBI.Common.Constants;
using BBI.Common.Enums;
using BBI.Domain;
using BBI.Framework.CustomFilters;
using BBI.Framework.GenericResponse;
using BBI.Framework.WebExtensions;
using BBI.Service.Blog;
using BBI.Service.Exception;
using BBI.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BBI.API
{
    [RoutePrefix("api")]
    [CustomExceptionFilter]
    public class BlogController : ApiController
    {
        private IBlogService _blogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public BlogController(IBlogService blogService)
        {
            _blogService = blogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Blog
        /// </summary>
        /// <returns></returns>
        [Route("Blogs")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAllBlogs([FromUri] SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
            	var blogs = _blogService.SelectBlog(param).Select(x => x.ToViewModel()); ;
            	return Ok(blogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get blog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("blog/{id:long}")]
        [HttpGet]
        public IHttpActionResult GetBlog(long id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.id = id;
	            var blog = _blogService.SelectBlog(param).FirstOrDefault().ToViewModel();
	            return Ok(blog.SuccessResponse());
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("blog")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult SaveBlog(BlogViewModel model)
        {
        	try
            {
	            model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _blogService.BlogInsert(model.ToModel());
	            return Ok(responseId.SuccessResponse("Blog save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("blog")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpPut]
        public IHttpActionResult UpdateBlog(BlogViewModel model)
        {
        	try{
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _blogService.BlogUpdate(model.ToModel());
	            return Ok("Blog Update successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete blog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("blog/{id:long}")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpDelete]
        public IHttpActionResult DeleteBlog(long id)
        {
        	try{
	            BlogViewModel model = new BlogViewModel();
	            model.Id = id;
	            model.IsDeleted = true;
	            _blogService.BlogUpdate(model.ToModel());
	            return Ok("Blog Deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Delete).ToString().ErrorResponse());
            }
        }
    }
}
