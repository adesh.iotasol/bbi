﻿using BBI.DBRepository.Blog;
using BBI.Domain.Blog;
using BBI.Service.Upload;
using BBI.Service.Xml;
using BBI.Service.FileGroup;
using BBI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Common.Extensions;

namespace BBI.Service.Blog
{
    public class BlogService : IBlogService
    {	
        public BlogDBService _blogDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        public BlogService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _blogDBService = new BlogDBService();
            _fileGroupService = fileGroupService;
            _xmlService = xmlService;
        }
        public long BlogInsert(BlogModel model)
        {
            var id = _blogDBService.BlogInsert(model);
            if (model.File != null && model.File.Path !=null)
            {
                //set target path and move file from target location
                model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, id);
                //Save list of file in our DB
                 model.File.CreatedBy = model.CreatedBy;
                 model.File.Type = "BLOG";
                 model.File.TypeId = id;
                _fileGroupService.FileGroupItemsInsert(model.File);
            }
            return id;
        }

        public void BlogUpdate(BlogModel model)
        {
            _blogDBService.BlogUpdate(model);
            if (model.File != null && model.File.Id == null && model.File.Path != null)
            {
                //set target path and move file from target location
                model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, model.Id);
                model.File.CreatedBy = model.CreatedBy;
                model.File.Type = "BLOG";
                model.File.TypeId = model.Id;
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsert(model.File);
            }
            
        }

        public List<BlogModel> SelectBlog(SearchParam param)
        {
            var response= _blogDBService.SelectBlog(param);
           
			response.ForEach(x =>
            {
             x.File = _xmlService.GetFileGroupItemsByXml(x.FileXml).FirstOrDefault();
            });
            return response;
        }
    }
}
