﻿using BBI.Domain.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.Service.Blog
{
    public interface IBlogService
    {
        /// <summary>
        /// used for insertion blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long BlogInsert(BlogModel model);
        
        /// <summary>
        /// Method for update blog
        /// </summary>
        /// <param name="model"></param>
        void BlogUpdate(BlogModel model);

        /// <summary>
        /// use to select all blog or select blog by id 
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<BlogModel> SelectBlog(SearchParam param);
    }
}
