﻿using BBI.Domain.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Users
{
    public interface IUserService
    {
        UserModel SelectUserByUniqueCode(string uniqueCode);
    }
}
