﻿using BBI.DBRepository.Category;
using BBI.Domain.Category;
using BBI.Service.Upload;
using BBI.Service.Xml;
using BBI.Service.FileGroup;
using BBI.Common.Extensions;
using BBI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Category
{
    public class CategoryService : ICategoryService
    {	
        public CategoryDBService _categoryDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        public CategoryService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _categoryDBService = new CategoryDBService();
            _fileGroupService = fileGroupService;
            _xmlService = xmlService;
        }
        public long CategoryInsert(CategoryModel model)
        {
            var id = _categoryDBService.CategoryInsert(model);
            return id;
        }

        public void CategoryUpdate(CategoryModel model)
        {
            _categoryDBService.CategoryUpdate(model);
            
        }

        public List<CategoryModel> SelectCategory(SearchParam param)
        {
            var response= _categoryDBService.SelectCategory(param);
           
			response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
