﻿using BBI.Domain.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.Service.Category
{
    public interface ICategoryService
    {
        /// <summary>
        /// used for insertion category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long CategoryInsert(CategoryModel model);
        
        /// <summary>
        /// Method for update category
        /// </summary>
        /// <param name="model"></param>
        void CategoryUpdate(CategoryModel model);

        /// <summary>
        /// use to select all category or select category by id 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<CategoryModel> SelectCategory(SearchParam param);
    }
}
