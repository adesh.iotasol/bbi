﻿using BBI.Domain;
using BBI.Domain.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Contact
{
    public interface IContactService
    {
        void ContactInsert(ContactModel model);
        List<ContactModel> SelectContact(SearchParam param);
    }
}
