﻿using BBI.DBRepository.Contact;
using BBI.Domain;
using BBI.Domain.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Contact
{
    public class ContactService: IContactService
    {
        private ContactDBService _contactDBService;
        public ContactService() {
            _contactDBService = new ContactDBService();
        }

        public void ContactInsert(ContactModel model) {
            _contactDBService.ContactInsert(model);
        }

        public List<ContactModel> SelectContact(SearchParam param)
        {
            var response = _contactDBService.SelectContact(param);

            response.ForEach(x =>
            {
            });
            return response;
        }
    }
}
