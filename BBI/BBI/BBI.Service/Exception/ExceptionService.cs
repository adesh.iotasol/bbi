﻿using BBI.DBRepository.Exception;
using BBI.Domain.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Exception
{
   public class ExceptionService: IExceptionService
    {
        public ExceptionDBService _ExceptionDBService;
        public ExceptionService()
        {
            _ExceptionDBService = new ExceptionDBService();
        }
        public void InsertLog(ExceptionModel exception )
        {
             _ExceptionDBService.ExceptionLogInsert(exception);
        }
    }
}
