﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain.DailyWorkSetting;
using BBI.DBRepository.DailyWorkSetting;

namespace BBI.Service.DailyWorkSetting
{
    public class DailyWorkSettingService : IDailyWorkSettingService
    {
        private DailyWorkSettingDBService _dailyWorkSettingDBService;
        public DailyWorkSettingService()
        {
            _dailyWorkSettingDBService = new DailyWorkSettingDBService();
        }
        public int DailyWorkSettingAction(DailyWorkSettingModel model)
        {
            return _dailyWorkSettingDBService.DailyWorkSettingAction(model);
        }

        public DailyWorkSettingModel DailyWorkSettingSelect()
        {
            return _dailyWorkSettingDBService.DailyWorkSettingSelect();
        }
    }
}
