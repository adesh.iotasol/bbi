﻿using BBI.Domain.DailyWorkSetting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.DailyWorkSetting
{
    public interface IDailyWorkSettingService
    {
        int DailyWorkSettingAction(DailyWorkSettingModel model);
        DailyWorkSettingModel DailyWorkSettingSelect();
    }
}
