﻿using BBI.DBRepository.Event;
using BBI.Domain.Event;
using BBI.Service.Upload;
using BBI.Service.Xml;
using BBI.Service.FileGroup;
using BBI.Common.Extensions;
using BBI.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Event
{
    public class EventService : IEventService
    {	
        public EventDBService _eventDBService;
        private IXmlService _xmlService;
        private IFileGroupService _fileGroupService;
        public EventService(IFileGroupService fileGroupService, IXmlService xmlService)
        {
            _eventDBService = new EventDBService();
            _fileGroupService = fileGroupService;
            _xmlService = xmlService;
        }
        public long EventInsert(EventModel model)
        {
            var id = _eventDBService.EventInsert(model);
            if (model.File != null && model.File.Id == null && model.File.Path != null)
            {
                //set target path and move file from target location
                model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, id);
                //Save list of file in our DB
                 model.File.CreatedBy = model.CreatedBy;
                 model.File.Type = "EVENT";
                model.File.TypeId = id;
                _fileGroupService.FileGroupItemsInsert(model.File);
            }
            return id;
        }

        public void EventUpdate(EventModel model)
        {
            _eventDBService.EventUpdate(model);
            if (model.File != null && model.File.Id == null && model.File.Path != null)
            {
                model.File = _fileGroupService.SetPathAndMoveSingleFile(model.File, model.Id);
                model.File.CreatedBy = model.CreatedBy;
                model.File.Type = "EVENT";
                model.File.TypeId = model.Id;
                //Save list of file in our DB
                _fileGroupService.FileGroupItemsInsert(model.File);
            }
            
        }

        public List<EventModel> SelectEvent(SearchParam param)
        {
            var response= _eventDBService.SelectEvent(param);
           
			response.ForEach(x =>
            {
             x.File = _xmlService.GetFileGroupItemsByXml(x.FileXml).FirstOrDefault();
            });
            return response;
        }
    }
}
