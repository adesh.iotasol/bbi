﻿using BBI.Domain.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.Service.Event
{
    public interface IEventService
    {
        /// <summary>
        /// used for insertion event
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        long EventInsert(EventModel model);
        
        /// <summary>
        /// Method for update event
        /// </summary>
        /// <param name="model"></param>
        void EventUpdate(EventModel model);

        /// <summary>
        /// use to select all event or select event by id 
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="isActive"></param>
        /// <param name="next"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<EventModel> SelectEvent(SearchParam param);
    }
}
