﻿using BBI.Domain.Upload;
﻿using BBI.Domain.Category;﻿using BBI.Domain.Blog;﻿using BBI.Domain.Event;//@@ADD_NEW_NAMESPACE_XML_CODE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.Service.Xml
{
    public interface IXmlService
    {
        List<FileGroupItemsModel> GetFileGroupItemsByXml(string attachmentFileXml);
        
        List<CategoryModel> GetCategoryByXml(string attachmentFileXml); List<BlogModel> GetBlogByXml(string attachmentFileXml); List<EventModel> GetEventByXml(string attachmentFileXml); //@@ADD_NEW_XML_CODE
    }
}



