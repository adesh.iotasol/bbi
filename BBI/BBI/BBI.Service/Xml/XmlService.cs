﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain.Upload;
using System.Xml.Linq;
using BBI.Common.Extensions;
﻿using BBI.Domain.Category;﻿using BBI.Domain.Blog;﻿using BBI.Domain.Event;//@@ADD_NEW_NAMESPACE_XML_CODE

namespace BBI.Service.Xml
{
    public class XmlService : IXmlService
    {
        #region Attachement file XML to List

        public List<FileGroupItemsModel> GetFileGroupItemsByXml(string attachmentFileXml)
        {
            if (string.IsNullOrEmpty(attachmentFileXml)) return new List<FileGroupItemsModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("FG").Select(x => new FileGroupItemsModel
            {
                Id = x.Element("Id").GetValue<long>(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                IsDeleted = x.Element("CreatedBy").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                Filename = x.Element("Filename").GetStrValue(),
                MimeType = x.Element("MimeType").GetStrValue(),
                Thumbnail = x.Element("Thumbnail").GetStrValue(),
                Path = x.Element("Path").GetStrValue(),
                OriginalName = x.Element("OriginalName").GetStrValue(),
                OnServer = x.Element("OnServer").GetStrValue(),
                TypeId = x.Element("TypeId").GetValue<long>(),
                Type = x.Element("Type").GetStrValue(),
                Size = x.Element("Size").GetValue<long>(),
            }).ToList();
        }
        
        public List<CategoryModel> GetCategoryByXml(string attachmentFileXml)
{
    if (string.IsNullOrEmpty(attachmentFileXml)) return new List<CategoryModel>();
    return XDocument.Parse(attachmentFileXml).Element("root").Elements("R").Select(x => new CategoryModel
    {
			  	Id = x.Element("Id").GetValue<long>(),
			  	CreatedBy = x.Element("CreatedBy").GetValue<long>(),
			  	UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
			  	CreatedOn = x.Element("CreatedOn").GetDateOffset(),
			  	UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
			  	IsDeleted = x.Element("IsDeleted").GetBoolVal(),
			  	IsActive = x.Element("IsActive").GetBoolVal(),
			  	Name = x.Element("Name").GetStrValue(),
    }).ToList();
}public List<BlogModel> GetBlogByXml(string attachmentFileXml)
{
    if (string.IsNullOrEmpty(attachmentFileXml)) return new List<BlogModel>();
    return XDocument.Parse(attachmentFileXml).Element("root").Elements("R").Select(x => new BlogModel
    {
			  	Id = x.Element("Id").GetValue<long>(),
			  	CreatedBy = x.Element("CreatedBy").GetValue<long>(),
			  	UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
			  	CreatedOn = x.Element("CreatedOn").GetDateOffset(),
			  	UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
			  	IsDeleted = x.Element("IsDeleted").GetBoolVal(),
			  	IsActive = x.Element("IsActive").GetBoolVal(),
			  	Title = x.Element("Title").GetStrValue(),
			  	Description = x.Element("Description").GetStrValue(),
				CategoryId = x.Element("CategoryId").GetValue<long>(),
    }).ToList();
}public List<EventModel> GetEventByXml(string attachmentFileXml)
{
    if (string.IsNullOrEmpty(attachmentFileXml)) return new List<EventModel>();
            return XDocument.Parse(attachmentFileXml).Element("root").Elements("R").Select(x => new EventModel
            {
                Id = x.Element("Id").GetValue<long>(),
                CreatedBy = x.Element("CreatedBy").GetValue<long>(),
                UpdatedBy = x.Element("UpdatedBy").GetValue<long>(),
                CreatedOn = x.Element("CreatedOn").GetDateOffset(),
                UpdatedOn = x.Element("UpdatedOn").GetDateOffset(),
                IsDeleted = x.Element("IsDeleted").GetBoolVal(),
                IsActive = x.Element("IsActive").GetBoolVal(),
                Title = x.Element("Title").GetStrValue(),
                Description = x.Element("Description").GetStrValue(),
                StartDate = x.Element("StartDate").GetDateOffset(),
                EndDate = x.Element("EndDate").GetDateOffset(),
                EventbriteLink = x.Element("EventbriteLink").GetStrValue(),
                VideoPath = x.Element("VideoPath").GetStrValue(),
                Address = x.Element("Address").GetStrValue(),
                EventTime = x.Element("EventTime").GetStrValue(),
    }).ToList();
}//@@ADD_NEW_XML_CODE
        #endregion
    }
}



