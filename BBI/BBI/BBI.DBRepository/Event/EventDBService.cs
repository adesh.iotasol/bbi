﻿using BBI.Domain.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.DBRepository.Event
{
    public class EventDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }

        public long EventInsert(EventModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.EventInsert(model.CreatedBy, model.UpdatedBy, model.IsActive, model.Title, model.Description, model.StartDate, model.EndDate,model.EventbriteLink,model.VideoPath, model.Address, model.EventTime).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void EventUpdate(EventModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.EventUpdate(model.Id, model.CreatedBy, model.UpdatedBy, model.CreatedOn, model.UpdatedOn, model.IsDeleted, model.IsActive, model.Title, model.Description, model.StartDate, model.EndDate, model.EventbriteLink,model.VideoPath,model.Address, model.EventTime);
            }

        }
        public List<EventModel> SelectEvent(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.EventSelect(param.Id, param.Type, param.IsActive, param.Next, param.Offset).Select(x => new EventModel
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Title = x.Title,
                    Description = x.Description,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    EventbriteLink = x.EventbriteLink,
                    FileXml = x.FileXml,
                    VideoPath = x.VideoPath,
                    Address = x.Address,
                    EventTime = x.EventTime,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                }).ToList();
            }
        }
    }
}
