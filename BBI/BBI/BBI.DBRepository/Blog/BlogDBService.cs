﻿using BBI.Domain.Blog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.DBRepository.Blog
{
    public class BlogDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }

        public long BlogInsert(BlogModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.BlogInsert(model.CreatedBy,model.UpdatedBy,model.IsActive,model.Title,model.Description,model.CategoryId,model.Slug, model.VideoPath,model.ShowOnHomePage).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void BlogUpdate(BlogModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.BlogUpdate(model.Id,model.CreatedBy,model.UpdatedBy,model.CreatedOn,model.UpdatedOn,model.IsDeleted,model.IsActive,model.Title,model.Description,model.CategoryId, model.VideoPath,model.ShowOnHomePage);
            }

        }
        public List<BlogModel> SelectBlog(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.BlogSelect(param.Id, param.Type, param.IsActive, param.Next, param.Offset,param.Slug, param.CategoryId, param.showOnHomepage).Select(x => new BlogModel
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy,
                    UpdatedBy = x.UpdatedBy,
                    CreatedOn = x.CreatedOn,
                    UpdatedOn = x.UpdatedOn,
                    IsDeleted = x.IsDeleted,
                    IsActive = x.IsActive,
                    Title = x.Title,
                    Description = x.Description,
                    CategoryId = x.CategoryId,
                    CategoryName = x.Name,
                    FileXml = x.FileXml,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    Slug = x.Slug,
                    VideoPath = x.VideoPath,
                    ShowOnHomePage = x.HomePageActive,
                }).ToList();
            }
        }
    }
}
