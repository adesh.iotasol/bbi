﻿using BBI.Domain.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBI.Domain;

namespace BBI.DBRepository.Category
{
    public class CategoryDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }

        public long CategoryInsert(CategoryModel model)
        {
            using (var dbctx = DbContext)
            {
                var id = dbctx.CategoryInsert(model.CreatedBy,model.UpdatedBy,model.IsActive,model.Name).FirstOrDefault();
                return Convert.ToInt64(id ?? 0);
            }

        }
        public void CategoryUpdate(CategoryModel model)
        {
            using (var dbctx = DbContext)
            {
                dbctx.CategoryUpdate(model.Id,model.CreatedBy,model.UpdatedBy,model.CreatedOn,model.UpdatedOn,model.IsDeleted,model.IsActive,model.Name);
            }

        }
        public List<CategoryModel> SelectCategory(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.CategorySelect(param.Id, param.IsActive, param.Next, param.Offset).Select(x => new CategoryModel
                {
	            			Id = x.Id,
	            			CreatedBy = x.CreatedBy,
	            			UpdatedBy = x.UpdatedBy,
	            			CreatedOn = x.CreatedOn,
	            			UpdatedOn = x.UpdatedOn,
	            			IsDeleted = x.IsDeleted,
	            			IsActive = x.IsActive,
	            			Name = x.Name,
                    TotalCount = x.overall_count.GetValueOrDefault(),
                    BlogCount = x.BlogCount
                }).ToList();
            }
        }
    }
}
