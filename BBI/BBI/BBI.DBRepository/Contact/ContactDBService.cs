﻿using BBI.Domain;
using BBI.Domain.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.DBRepository.Contact
{
    public class ContactDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }
        public void ContactInsert(ContactModel model) {
            using (var dbctx = DbContext)
            {
                dbctx.ContactInsert(model.Name, model.Email, model.Subject, model.Message);
            }
        }

        public List<ContactModel> SelectContact(SearchParam param)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.ContactSelect(param.Id).Select(x => new ContactModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    Subject = x.Subject,
                    Message = x.Message,
                    CreatedOn = x.CreatedOn
                }).ToList();
            }
        }
    }
}
