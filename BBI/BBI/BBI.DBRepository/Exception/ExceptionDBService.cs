﻿using BBI.Domain.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.DBRepository.Exception
{
   public class ExceptionDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }
        public void ExceptionLogInsert(ExceptionModel exception)
        {
            using (var dbctx = DbContext)
            {
                dbctx.InsertExceptionLog(exception.Source, exception.Message, exception.StackTrace, exception.Uri, exception.Method, exception.CreatedBy);
                
            }
        }
    }
}
