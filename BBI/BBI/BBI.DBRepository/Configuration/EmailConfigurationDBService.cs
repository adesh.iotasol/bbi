using BBI.DBRepository;
﻿using BBI.Domain.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBI.DBRepository.Configuration
{
    public class EmailConfigurationDBService
    {
        BBIDbEntities DbContext { get { return new BBIDbEntities(); } }
        public EmailConfigurationModel EmailConfigurationSelect(string configurationKey)
        {
            using (var dbctx = DbContext)
            {
                return dbctx.EmailConfigurationSelect(configurationKey).Select(x => new EmailConfigurationModel
                {
                    ConfigurationKey = x.ConfigurationKey,
                    ConfigurationValue = x.ConfigurationValue,
                    EmailSubject = x.EmailSubject,
                    Id = x.Id
                }).FirstOrDefault();
            }
        }
    }
}
