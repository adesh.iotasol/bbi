﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BBI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "dashboard",
              url: "dashboard",
              defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "home",
              url: "home",
              defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "Community",
              url: "community",
              defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "CommunityEventDetail",
             url: "community-event-detail/{id}",
             defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "Incubation",
             url: "incubation",
             defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "Contact",
             url: "contact",
             defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "Stories",
             url: "stories",
             defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "StoriesDetails",
             url: "stories/details/{slug}",
             defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );

            routes.MapRoute(
            name: "Thankyou",
            url: "thankyou",
            defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
        );

            routes.MapRoute(
            name: "OfficeSpace",
            url: "office-space",
            defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
        );

            routes.MapRoute(
            name: "CoWorking",
            url: "co-working",
            defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
        );

            routes.MapRoute(
                name: "changePassword",
                url: "dashboard/change/password",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "recoverPassword",
                url: "recoverPassword",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "accountSettings",
                url: "dashboard/account/settings",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "signIn",
                url: "signIn",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "404",
                url: "404",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "403",
                url: "403",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CategoryList",
                url: "dashboard/category-list",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "CategoryEdit",
                url: "dashboard/category/edit/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "CategoryDetail",
                url: "dashboard/category/detail/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "BlogList",
                url: "dashboard/blog-list",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "BlogEdit",
                url: "dashboard/blog/edit/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "BlogDetail",
                url: "dashboard/blog/detail/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "EventList",
                url: "dashboard/event-list",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "EventListSection",
               url: "event-section/{scrollTo}",
               defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "EventEdit",
                url: "dashboard/event/edit/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "EventDetail",
                url: "dashboard/event/detail/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ContactList",
                url: "dashboard/contact-list",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Site", id = UrlParameter.Optional }
            );
        }
    }
}
