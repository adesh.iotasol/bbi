﻿using BBI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace BBI.Providers
{
    public class ManageClaims
    {
        public static UserClaim GetUserClaim()
        {
            ClaimsIdentity claimsIdentity = System.Web.HttpContext.Current.User.Identity as ClaimsIdentity;
            UserClaim userClaim = new UserClaim();
            var claims = claimsIdentity.Claims.Select(x => new { type = x.Type, value = x.Value });
            userClaim.TenantId = Convert.ToInt32(claims.Where(m => m.type == "TanantId").FirstOrDefault().value);
            return userClaim;
        }
    }
}
