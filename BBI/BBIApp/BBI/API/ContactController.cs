﻿using BBI.Providers;
using BBI.Service.Contact;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using BBI.Domain.Contact;
using BBI.Framework.GenericResponse;
using BBI.Framework.ViewModels.Contact;
using BBI.Service.Configuration;
using Microsoft.AspNet.Identity;
using BBI.Common.Enums;
using BBI.Common.Constants;
using BBI.Core.Mailer;
using BBI.Domain;
using IotaSmartBuild.Common.Enums;

namespace BBI.API
{
    [RoutePrefix("api")]
    public class ContactController : ApiController
    {
        private IContactService _contactService;
        private IEmailConfigurationService _emailConfiguration;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public ContactController(IContactService contactService, IEmailConfigurationService emailConfiguration)
        {
            _contactService = contactService;
            _emailConfiguration = emailConfiguration;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }
        ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        [Route("contacts")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAllContacts([FromUri] SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                var contacts = _contactService.SelectContact(param).Select(x => x.ToViewModel()); ;
                return Ok(contacts.SuccessResponse());
            }
            catch (Exception ex)
            {
                return Ok(ex.ErrorResponse());
            }
        }

        [Route("contact/insert")]
        [HttpPost]
        public IHttpActionResult ContactInsert(ContactViewModel model)
        {
            try
            {
                _contactService.ContactInsert(model.ToModel());
                SendEmailForEmailConfirm(model.Email, model.Name, model.Subject, model.Message);
                return Ok("Form submitted successfuly");

            }
            catch (Exception ex)
            {
                return Ok(ex.Message.ErrorResponse());
            }
        }


        public void SendEmailForEmailConfirm(string toEmail, string name, string subject, string message)
        {

            var configData = _emailConfiguration.EmailConfigurationSelect(EmailConfigurationKey.ContactEnquiry.ToString());
            var emailBody = string.Format(configData.ConfigurationValue, name, toEmail, subject, message);
            MailSender.SendEmail("sandeep@iotasol.com", EmailConstants.USER_CONTACT, emailBody).Wait();
        }

    }
}