using BBI.Framework.ViewModels.Event;
using BBI.Common.Constants;
using BBI.Common.Enums;
using BBI.Domain;
using BBI.Framework.CustomFilters;
using BBI.Framework.GenericResponse;
using BBI.Framework.WebExtensions;
using BBI.Service.Event;
using BBI.Service.Exception;
using BBI.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IotaSmartBuild.Common.Enums;

namespace BBI.API
{
    [RoutePrefix("api")]
    [CustomExceptionFilter]
    public class EventController : ApiController
    {
        private IEventService _eventService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public EventController(IEventService eventService, IExceptionService exceptionService) 
        {
            _eventService = eventService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Event
        /// </summary>
        /// <returns></returns>
        [Route("Events")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAllEvents([FromUri] SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
                param.Type = "EVENT";
                var events = _eventService.SelectEvent(param).Select(x => x.ToViewModel()); ;
            	return Ok(events.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Select).ToString().ErrorResponse());
            }
        }

        [Route("active-event")]
        [HttpGet]
        public IHttpActionResult GetAllActiveEvents([FromUri] SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                param.Type = "EVENT";
                param.IsActive = true;
                var events = _eventService.SelectEvent(param).Select(x => x.ToViewModel()); ;
                return Ok(events.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get event by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("event/{id:long}")]
        [HttpGet]
        public IHttpActionResult GetEvent(long id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;
                param.Type = "EVENT";
                var record = _eventService.SelectEvent(param).FirstOrDefault().ToViewModel();
	            return Ok(record.SuccessResponse());
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Single_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save event
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("event")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult SaveEvent(EventViewModel model)
        {
        	try
            {
                SearchParam param = new SearchParam();
                param.Type = "EVENT";
                var events = _eventService.SelectEvent(param);

                model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _eventService.EventInsert(model.ToModel());
	            return Ok(responseId.SuccessResponse("Event save successfully"));
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Insert);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update event
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("event")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpPut]
        public IHttpActionResult UpdateEvent(EventViewModel model)
        {
        	try{
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _eventService.EventUpdate(model.ToModel());
	            return Ok("Event updated successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Update);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete event by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("event/{id:long}")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpDelete]
        public IHttpActionResult DeleteEvent(long id)
        {
        	try{
	            EventViewModel model = new EventViewModel();
	            model.Id = id;
	            model.IsDeleted = true;
	            _eventService.EventUpdate(model.ToModel());
	            return Ok("Event deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Event, (int)ErrorFunctionCode.Event_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Event_Delete).ToString().ErrorResponse());
            }
        }
    }
}
