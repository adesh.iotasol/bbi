using BBI.Framework.ViewModels.Blog;
using BBI.Common.Constants;
using BBI.Common.Enums;
using BBI.Domain;
using BBI.Framework.CustomFilters;
using BBI.Framework.GenericResponse;
using BBI.Framework.WebExtensions;
using BBI.Service.Blog;
using BBI.Service.Exception;
using BBI.Providers;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IotaSmartBuild.Common.Enums;
using BBI.Common.Extensions;

namespace BBI.API
{
    [RoutePrefix("api")]
    [CustomExceptionFilter]
    public class BlogController : ApiController
    {
        private IBlogService _blogService;
        private IExceptionService _exceptionService;
        private ManageApplicationUserModel _manageApplicationUserModel;
        public BlogController(IBlogService blogService, IExceptionService exceptionService)
        {
            _blogService = blogService;
            _exceptionService = exceptionService;
            _manageApplicationUserModel = new ManageApplicationUserModel();
        }

        /// <summary>
        ///  Api use for get all Blog
        /// </summary>
        /// <returns></returns> 
        [Route("blogs")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpGet]
        public IHttpActionResult GetAllBlogs([FromUri] SearchParam param)
        {
        	try{
        		param = param ?? new SearchParam();
                param.Type = "BLOG";
                var blogs = _blogService.SelectBlog(param).Select(x => x.ToViewModel()); ;
            	return Ok(blogs.SuccessResponse());
            }catch(Exception ex){
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Select).ToString().ErrorResponse());
            }
        }

        [Route("homepage/active")]
        [HttpGet]
        public IHttpActionResult GetAllHomepageBloges([FromUri] SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                param.Type = "BLOG";
                param.showOnHomepage = true;
                param.IsActive = true;
                var blogs = _blogService.SelectBlog(param).Select(x => x.ToViewModel()); ;
                return Ok(blogs.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for get blog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("blog/{id:long}")]
        [HttpGet]
        public IHttpActionResult GetBlog(long id)
        {
        	try{
	        	SearchParam param = new SearchParam();
	            param.Id = id;
                param.Type = "BLOG";
                var blog = _blogService.SelectBlog(param).FirstOrDefault().ToViewModel();
	            return Ok(blog.SuccessResponse());
            }
            catch (Exception ex)
            {
                 var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Single_Select).ToString().ErrorResponse());
            }
        }

        [Route("active-blog/{slug}")]
        [HttpGet]
        public IHttpActionResult GetActiveBlog(string slug)
        {
            try
            {
                SearchParam param = new SearchParam();
                param.Slug = slug;
                param.Type = "BLOG";
                var blog = _blogService.SelectBlog(param).FirstOrDefault().ToViewModel();
                return Ok(blog.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Single_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Single_Select).ToString().ErrorResponse());
            }
        }

        [Route("active-blogs")]
        [HttpGet]
        public IHttpActionResult GetAllActiveBlogs([FromUri] SearchParam param)
        {
            try
            {
                param = param ?? new SearchParam();
                param.Type = "BLOG";
                param.IsActive = true;
                var blogs = _blogService.SelectBlog(param).Select(x => x.ToViewModel()); ;
                return Ok(blogs.SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Select);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Select).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for  save blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("blog")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [HttpPost]
        public IHttpActionResult SaveBlog(BlogViewModel model)
        {
        	try
            {
                SearchParam param = new SearchParam();
                param.Type = "BLOG";
                param.Slug = StringExtensions.ToSlug(model.Title);
               var blogs = _blogService.SelectBlog(param);
                model.Slug = param.Slug;
                if (blogs.Count > 0) {
                    model.Slug = param.Slug + "_" + (blogs.Count + 1);
                }

                model.CreatedBy = model.UpdatedBy= User.Identity.GetUserId<long>();
	            var responseId = _blogService.BlogInsert(model.ToModel());
	            return Ok(responseId.SuccessResponse("Blog save successfully"));
            }
            catch (Exception ex)
            {
                //var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Insert);
                //_exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Insert).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for update blog
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("blog")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpPut]
        public IHttpActionResult UpdateBlog(BlogViewModel model)
        {
        	try{
        		model.UpdatedBy = User.Identity.GetUserId<long>();
	            _blogService.BlogUpdate(model.ToModel());
	            return Ok("Blog updated successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                //var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Update);
                //_exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Update).ToString().ErrorResponse());
            }
        }

        /// <summary>
        /// Api use for delete blog by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("blog/{id:long}")]
        [Authorize(Roles = "ROLE_ADMIN")]
        [CustomExceptionFilter]
        [HttpDelete]
        public IHttpActionResult DeleteBlog(long id)
        {
        	try{
	            BlogViewModel model = new BlogViewModel();
	            model.Id = id;
	            model.IsDeleted = true;
	            _blogService.BlogUpdate(model.ToModel());
	            return Ok("Blog deleted successfully".SuccessResponse());
            }
            catch (Exception ex)
            {
                var exception = _manageApplicationUserModel.ExceptionToExceptionModel(ex, (int)ErrorCode.Blog, (int)ErrorFunctionCode.Blog_Delete);
                _exceptionService.InsertLog(exception);
                return Ok(((int)ErrorFunctionCode.Blog_Delete).ToString().ErrorResponse());
            }
        }
    }
}
