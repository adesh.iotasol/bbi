var apiBaseUrl = '/';
var secureApiBaseUrl = apiBaseUrl + "api/";
var publicApiBaseUrl = apiBaseUrl;

angular.module('applicationApp-directives', []);
(function () {
    var applicationApp = angular.module("applicationApp", ["ui.router", "common.services", "angularUtils.directives.dirPagination", 'LocalStorageModule', 'ui.bootstrap', 'angularFileUpload', 'pascalprecht.translate', 'applicationApp-directives']);
    applicationApp.config(applicationAppConfig);
    applicationApp.run(applicationAppRun);

    applicationAppConfig.$inject = ["$stateProvider", "$httpProvider", "$urlRouterProvider", "$locationProvider", "paginationTemplateProvider", "$translateProvider"];
    applicationAppRun.$inject = ["$state", "$rootScope", "AuthServerProvider", "AccountRepository"];

    function applicationAppConfig($stateProvider, $httpProvider, $urlRouterProvider, $locationProvider, paginationTemplateProvider, $translateProvider) {

        $translateProvider.useStaticFilesLoader({
            prefix: 'wwwroot/localization/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en-US');
        paginationTemplateProvider.setPath('wwwroot/js/dirPagination.tpl.html');

        $stateProvider.state('base', {
            abstract: true,
            url: '',
            templateUrl: '/wwwroot/angular/layouts/base.html?v=' + window.app_version,
        });

        $stateProvider.state('siteLayout', {
            abstract: true,
            url: '',
            templateUrl: '/wwwroot/angular/layouts/siteLayout.html',
            controller: "HeaderController",
            controllerAs: "vm"
        });


        $stateProvider.state("default", {
            url: "/",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/home/home.html",
            controller: "HomeController",
            controllerAs: "vm"
        });

        $stateProvider.state("home", {
            url: "/home",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/home/home.html",
            controller: "HomeController",
            controllerAs: "vm"
        });

        $stateProvider.state("Community", {
            url: "/community",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/community/community.html",
            controller: "CommunityController",
            controllerAs: "vm",
            resolve: {}
        });

        $stateProvider.state("CommunityEventDetail", {
            url: "/community-event-detail/:id",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/communityEventDetail/community-event-detail.html",
            controller: "CommunityEventDetailsController",
            controllerAs: "vm",
            resolve: {}
        });

        $stateProvider.state("Contact", {
            url: "/contact",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/contact/contact.html"
        });

        $stateProvider.state("Incubation", {
            url: "/incubation",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/incubation/incubation.html"
        });

        $stateProvider.state("Stories", {
            url: "/stories",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/stories/Stories.html",
            controller: "StoriesController",
            controllerAs: "vm",
            resolve: {}
        });

        $stateProvider.state("StoriesDetails", {
            url: "/stories/details/:slug",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/stories/StoriesDetails.html",
            controller: "StoriesDetailsController",
            controllerAs: "vm",
            resolve: {}
        });

        $stateProvider.state("OfficeSpace", {
            url: "/office-space",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/officeSpace/OfficeSpace.html"
        });

        $stateProvider.state("CoWorking", {
            url: "/co-working",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/coWorking/CoWorking.html"
        });

        $stateProvider.state("Thankyou", {
            url: "/thankyou",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/thankyou/thankyou.html"
        });

        $stateProvider.state("signIn", {
            url: "/signIn",
            parent: 'base',
            templateUrl: "/wwwroot/angular/signIn/signIn.html?v=" + window.app_version,
            controller: 'SignInController',
            controllerAs: 'vm',
            resolve: {}
        });

        $stateProvider.state("recoverPassword", {
            url: "/account/recover/:ucode",
            parent: 'base',
            templateUrl: "/wwwroot/angular/account/recoverPassword.html?v=" + window.app_version,
            controller: "RecoverPasswordController",
            controllerAs: "vm"
        });

        $stateProvider.state('dashboard', {
            url: '/dashboard',
            templateUrl: '/wwwroot/angular/layouts/dashboard.html?v=' + window.app_version,
            controller: 'HeaderController',
            controllerAs: 'vm',
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("changePassword", {
            url: "/change/password",
            parent: 'dashboard',
            templateUrl: "/wwwroot/angular/account/changePassword.html?v=" + window.app_version,
            controller: "ChangePasswordController",
            controllerAs: "vm"
        });


        $stateProvider.state("accountSettings", {
            url: "/account/settings",
            parent: 'dashboard',
            templateUrl: "/wwwroot/angular/account/accountSettings.html?v=" + window.app_version,
            controller: "AccountSettingsController",
            controllerAs: "vm"
        });

        $stateProvider.state("404", {
            url: "/404",
            parent: 'base',
            templateUrl: "/wwwroot/angular/error/404.html?v=" + window.app_version,
            resolve: {}
        });

        $stateProvider.state("403", {
            url: "/403",
            parent: 'base',
            templateUrl: "/wwwroot/angular/error/403.html?v=" + window.app_version,
            resolve: {}
        });
        $stateProvider.state("CategoryList", {
            url: "/category-list",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Category/CategoryList.html",
            controller: "CategoryListController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("CategoryEdit", {
            url: "/category/edit/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Category/CategoryEdit.html",
            controller: "CategoryEditController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("CategoryDetail", {
            url: "/category/detail/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Category/CategoryDetail.html",
            controller: "CategoryDetailController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });
        $stateProvider.state("BlogList", {
            url: "/blog-list",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Blog/BlogList.html",
            controller: "BlogListController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("BlogEdit", {
            url: "/blog/edit/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Blog/BlogEdit.html",
            controller: "BlogEditController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("BlogDetail", {
            url: "/blog/detail/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Blog/BlogDetail.html",
            controller: "BlogDetailController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });
        $stateProvider.state("EventList", {
            url: "/event-list",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Event/EventList.html",
            controller: "EventListController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("EventListSection", {
            url: "/event-section/:scrollTo",
            parent: 'siteLayout',
            templateUrl: "/wwwroot/angular/site/pages/community/community.html",
            controller: "CommunityController",
            controllerAs: "vm",
        });

        $stateProvider.state("EventEdit", {
            url: "/event/edit/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Event/EventEdit.html",
            controller: "EventEditController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("EventDetail", {
            url: "/event/detail/:id",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Event/EventDetail.html",
            controller: "EventDetailController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        $stateProvider.state("ContactList", {
            url: "/contact-list",
            parent: 'dashboard',
            templateUrl: "wwwroot/angular/pages/Contact/ContactList.html",
            controller: "ContactListController",
            controllerAs: "vm",
            resolve: {
                authServerProvider: "AuthServerProvider",
                authorize: function (authServerProvider, $q) {
                    return authServerProvider.isAuthorizedRoleUser(['ROLE_ADMIN'], $q);
                }
            }
        });

        if (window.history && window.history.pushState) {
            $locationProvider.html5Mode(true);
        }

        $urlRouterProvider.otherwise("404");
        $httpProvider.interceptors.push('authInterceptor');
        $httpProvider.interceptors.push('authExpiredInterceptor');
    }

    function applicationAppRun($state, $rootScope, AuthServerProvider, AccountRepository) {
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            $rootScope._viewPage = toState.url.split("/")[1];
            $(window).scrollTop(0);
            AuthServerProvider.authenticate();
        });

        $rootScope.signOff = signOff;

        function signOff() {
            AuthServerProvider.signOff();
        }

        // Call when the 401 response is returned by the client
        $rootScope.$on('event:auth-authRequired', function () {
            $rootScope.authenticated = true;
        });

        // Call when the user logs out
        $rootScope.$on('event:auth-authConfirmed', function () {
            $rootScope.user = AuthServerProvider.getUser();
            $rootScope.authenticated = true;
        });
    }

    applicationApp.filter('dateTimeFormat', function () {
        return function (dateString) {
            if (dateString == "" || dateString == null || dateString == "-")
                return "-";
            return moment(dateString).format('MMM DD');
        }
    });

    applicationApp.filter('dateFilter', function ($filter) {
        var angularDateFilter = $filter('date');
        return function (date) {
            return angularDateFilter(date, 'dd/MM/yyyy');
        }
    });
    applicationApp.filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
    applicationApp.directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit(attr.broadcasteventname ? attr.broadcasteventname : 'ngRepeatFinished');
                    });
                }
            }
        }
    });
}());