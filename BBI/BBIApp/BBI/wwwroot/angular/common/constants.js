(function () {
    var constantsService = angular.module("applicationApp");
    constantsService.constant("CONSTANTS", {  
    	IS_PRODUCTION: false,
        LANG_ENGLISH: "en-US",
        LANG_ITALIAN: "it-IT",
        DEFAULT_LANGUAGE: "en-US",
        VIEW_USER_MAPPING : {
             		"CategoryAddButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "CategoryEditButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "CategoryDeleteButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "CategoryCancelButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "CategorySelectButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "CategoryList": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "CategoryEditFields": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                }, 
        		"BlogAddButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "BlogEditButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "BlogDeleteButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "BlogCancelButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "BlogSelectButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "BlogList": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "BlogEditFields": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                }, 
        		"EventAddButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "EventEditButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "EventDeleteButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "EventCancelButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "EventSelectButton": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },
                "EventList": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                },  
                "EventEditFields": {
                    SHOW_TO_ROLE: ["ROLE_ADMIN"],
                    ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
                }, 
           		"accountSettings": {
		            SHOW_TO_ROLE: ["ROLE_ADMIN"],
		            ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
		        },
		        "changePassword": {
		            SHOW_TO_ROLE: ["ROLE_ADMIN"],
		            ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
		        },
		         "logout": {
		            SHOW_TO_ROLE: ["ROLE_ADMIN"],
		            ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
		        },
		         "ContactList": {
		            SHOW_TO_ROLE: ["ROLE_ADMIN"],
		            ENABLED_FOR_ROLE: ["ROLE_ADMIN"],
		        }
            }
    });
        
}());

