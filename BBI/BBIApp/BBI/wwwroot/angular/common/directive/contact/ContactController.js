﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("ContactController", contactController);
    contactController.$inject = ["$filter", "$state", "$stateParams", "HeaderRepository"];

    function contactController($filter, $state, $stateParams, HeaderRepository) {

        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        function init() {
            vm.record = {};
        }
        init();

        function save(bol) {
            vm.onClickValidation = !bol;
            if (!bol) {
                return;
            }
            vm.disableButton = true;
            HeaderRepository.save(vm.record, function (data) {
                showTost("Success:", "Form submitted successfuly", "success");
                vm.cancel();
                vm.disableButton = false;
            }, function (error) {
                showTost("Error:", error.data.message, "danger");
                vm.disableButton = false;
            });
        }

        function cancel() {
            $state.go("Thankyou");
        }

    }
}());