﻿angular.module('applicationApp-directives')
.directive('contactForm', function () {
    return {
        restrict: 'EA',
        scope:{},
        templateUrl: 'wwwroot/angular/common/directive/contact/contact.html',
        controller: 'ContactController',
        controllerAs: 'vm'

    }
})