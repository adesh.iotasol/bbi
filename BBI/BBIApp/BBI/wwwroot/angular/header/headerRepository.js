﻿(function () {
    var commonService = angular.module("common.services");
    commonService.factory("HeaderRepository", headerRepository);
    headerRepository.$inject = ["$resource"];

    function headerRepository($resource) {
        return $resource(apiBaseUrl + "api/", {}, {
            save: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "contact/insert"
            },
            fetchAll: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "contacts"
            },
        })
    }
}());