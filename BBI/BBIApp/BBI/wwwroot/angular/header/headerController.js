(function () {
    "use strict";

    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("HeaderController", headerController);

    headerController.$inject = ["$scope", "CommonUtils", "AuthServerProvider","HeaderRepository"];

    function headerController($scope, CommonUtils, AuthRepository, HeaderRepository) {
        var vm = this;
        vm.changeLanguage = changeLanguage;
        vm.getChosenLanguage = getChosenLanguage;
        vm.authRepository = AuthRepository;
        vm.contactAdd = contactAdd;
        vm.record = {};
        init();

        function init() {
        }

        function changeLanguage(language) {
            CommonUtils.setChosenLanguage(language);
        }

        function getChosenLanguage() {
            return CommonUtils.getChosenLanguage();
        }
        function contactAdd() {
            HeaderRepository.save(vm.record, function (data) {
                if (!data.status) {
                    showTost("Error:", data.message, "danger");
                    return;
                }
                showTost("Success:", data.message, "success");
                
            });
            
        }

    }
}());
