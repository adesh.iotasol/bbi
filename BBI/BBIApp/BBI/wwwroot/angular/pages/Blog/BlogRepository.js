(function () {
    var commonService = angular.module("common.services");
    commonService.factory("BlogRepository", blogRepository);
    blogRepository.$inject = ["$resource"];

    function blogRepository($resource) {
        return $resource(apiBaseUrl + "api/", {}, {
            fetchAll: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "blogs"
            },
            fetch: {
                method: 'GET',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "blog/:id"
            },
            save: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "blog"
            },
            update: {
                method: 'PUT',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "blog"
            },
            remove: {
                method: 'DELETE',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "blog/:id"
            },
            removeAll: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "blog/remove"
            },
            removeBlogImage: {
                method: 'DELETE',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "file/group/item/delete/:id"
            }
        });
    }
}());
