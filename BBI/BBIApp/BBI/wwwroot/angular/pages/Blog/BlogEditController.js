(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("BlogEditController", blogEditController);
    blogEditController.$inject = ["$scope", "$state", "$timeout", "BlogRepository", "$filter", "FileUploader", "$stateParams", "CategoryRepository", "AuthServerProvider"];

    function blogEditController($scope, $state, $timeout, AccessRepository, $filter, FileUploader, $stateParams, CategoryRepository, AuthRepository) {

        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.remove = remove;
        vm.type = '';
        vm.authRepository = AuthRepository;
        vm.uploadFile = uploadFile;
        vm.removeBlogImage = removeBlogImage;
        vm.getMediaType = getMediaType;
        init();

        function init() {
            vm.record = {};
            vm.recordId = $stateParams.id;
            vm.isNewRecord = true;
            CategoryRepository.fetchAll(function (response) {
                vm.categoryList = response.data;
            },
                function (error) {
                    CommonUtils.logging("Error", "Unable to Fetch Associated Table", "Init()", "blogEditController.js");
                });
            vm.record.isDeleted = false;
            vm.record.isActive = true;
            vm.record.showOnHomePage = true;
            if (vm.recordId <= 0) {
                autoSelected();
                return;
            }
            AccessRepository.fetch({
                "id": vm.recordId
            }, function (response) {
                vm.loading = false;
                vm.isNewRecord = false;
                vm.record = response.data;
                if (vm.record.videoPath.length > 0) {
                    vm.mediaType = "video";
                    vm.type = "video";
                } else {
                    vm.mediaType = "image";
                    vm.type = "image";
                }
                postprocessing();

            },
            function (error) {
                vm.errorResponse = error.data;
                vm.isError = true;
                vm.loading = false;
            });
        }

        function getMediaType(type) {
            type == 'image' ? vm.record.videoPath = '' : vm.record.file = [];
            vm.type = type;
        }

        function postprocessing() {
            autoSelected();
            setTimeout(function () {
                $('.categoryId').val(vm.record.categoryId);
                $('.categoryId').trigger('change');
            }, 300);
        }

        function uploadFile() {
            $("#fileUpload").click();
        }


        function autoSelected(value) {
            $('.categoryId').select2();
        }

        function save(bol) {
            vm.onClickValidation = !bol;
            if (!bol) {
                return;
            }

            if (vm.type.length <= 0) {
                showTost("Error:", "Please select video or image option", "danger");
                return;
            }

            if ((vm.record.videoPath == '') && (!vm.record.file) && (vm.type == 'image')) {
                showTost("Error:", "Please upload image", "danger");
                return;
            }
            if (!vm.record.videoPath && vm.type == 'video') {
                showTost("Error:", "Please upload video", "danger");
                return;
            }
            if (!vm.record.videoPath ==  '') {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var url = vm.record.videoPath;
                var match = url.match(regExp);
                if (!match || !vm.record.videoPath.includes("youtu")) {
                    showTost("Error:", "Invalid Youtube Link", "danger");
                    return;
                }
                vm.record.videoPath = `https://www.youtube.com/embed/${match[2]}`;

            }

            var tempRecord = angular.copy(vm.record);

            var method = "save";
            if (!vm.isNewRecord) {
                method = "update"
            }
            AccessRepository[method](tempRecord, function (data) {
                if (!data.status) {
                    showTost("Error:", data.message, "danger");
                    return;
                }
                showTost("Success:", data.message, "success");
                vm.cancel();
            }, function (error) {
                showTost("Error:", error.data.message, "danger");
            });
        }

        function cancel() {
            $state.go("BlogList");
        }

        function removeBlogImage() {
            customDeleteConfirmation(removeBlogImageCallback, vm.record.file);
        }

        function removeBlogImageCallback(obj) {
            if (!obj.id) {
                $scope.$apply(function () {
                    vm.record.file = {};
                    sweetAlert.close();
                    showTost("Success:", "Blog Image Removed Successfully", "success");
                });
                return;
            }
            AccessRepository.removeBlogImage({
                id: obj.id
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                vm.record.file = {};
                showTost("Success:", "Blog Image Removed Successfully", "success");
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }


        function remove() {
            customDeleteConfirmation(removeRecordCallback, vm.recordId);
        }

        function removeRecordCallback(recordId) {
            AccessRepository.remove({
                id: recordId
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                showTost("Success:", "Record Deleted Successfully", "success");
                vm.cancel();
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }

        vm.file_uploader = new FileUploader({
            url: secureApiBaseUrl + 'file/group/items/upload',
            autoUpload: true
        });
        vm.file_uploader.onSuccessItem = function (fileItem, response, status, headers) {
            vm.record.file = response[0];
        };

        vm.loadAssociatedPopup = loadAssociatedPopup;

        function loadAssociatedPopup(name, fieldName) {
            vm.associated = {
                isActive: true
            };
            if (fieldName === 'CategoryId') {
                vm.repository = CategoryRepository;
            }
            vm.loadAssociatedAll(fieldName);
            $("#" + name).modal("show");

        }
        vm.onSelectAssociated = onSelectAssociated;

        function onSelectAssociated(value, name, popUpName) {
            vm.record[name] = value;
            setTimeout(function () {
                $('.' + name).val(value).trigger("change");
            });
            vm.associated = undefined;
            $("#" + popUpName).modal("hide");
        }

        vm.onRemoveAssociated = onRemoveAssociated;

        function onRemoveAssociated(id, fieldName) {
            customDeleteConfirmation(removeAssociatedRecordCallback, id, fieldName);
        }

        function removeAssociatedRecordCallback(recordId, fieldName) {
            vm.repository.remove({
                id: recordId
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                showTost("Success:", "Record Deleted Successfully", "success");
                vm.loadAssociatedAll(fieldName);
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }
        vm.loadAssociatedAll = loadAssociatedAll;

        function loadAssociatedAll(fieldName) {
            vm.repository.fetchAll(function (response) {
                if (fieldName === 'CategoryId') {
                    vm.categoryList = response.data;
                }
            });
        }
        vm.onSaveAssociated = onSaveAssociated;

        function onSaveAssociated(fieldName) {
            vm.repository.save(vm.associated, function (response) {
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                vm.associated = {
                    isActive: true
                };
                vm.showAddForm = false;
                vm.loadAssociatedAll(fieldName);
            });
        }
    }
}());

