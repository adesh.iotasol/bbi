(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("BlogListController", blogListController);
    blogListController.$inject = ["$state", "$timeout", "BlogRepository", "$filter", "$scope", "CommonUtils", "AuthServerProvider"];

    function blogListController($state, $timeout, RecordRepository, $filter, $scope, CommonUtils, AuthRepository) {

        var vm = this;
        vm.remove = remove;
        vm.authRepository = AuthRepository;
        vm.recordStatus = recordStatus;
        vm.homePageStatus = homePageStatus;
		
        function init() {
            vm.loading = true;
            vm.recordList = [];
            RecordRepository.fetchAll(function (response) {
                vm.recordList = response.data;
                    vm.loading = false;
                },
                function (error) {
                    vm.errorResponse = error.data;
                    vm.isError = true;
                    vm.loading = false;
                    return;
                });
        }
		init();
		
        $scope.$on('ngRepeatBroadcast1', function () {
            $timeout(function(){
                $('#sampleTable').DataTable({
	                "destroy": true
	            });
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        function remove(id) {
            customDeleteConfirmation(removeRecordCallback,id);
        }

        function removeRecordCallback(id) {
            RecordRepository.remove({id:id}, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                showTost("Success:", "Record Deleted Successfully", "success");
                init();
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Somethings went wrong.", "danger");
            });
        }

        function recordStatus(status, id) {
            var obj = {};
            obj.isActive = status;
            obj.id = id;
            RecordRepository.update(obj, function (response) {
                sweetAlert.close();
                showTost("Success:", "Record updated successfully", "success");
            }, function (error) {
            });
        }

        function homePageStatus(id,status){
            RecordRepository.update({id: id, showOnHomePage: status }, function (response) {
                sweetAlert.close();
                showTost("Success:", "Record updated successfully", "success");
            }, function (error) {
            });
        }
    }
}());
