(function () {
    var commonService = angular.module("common.services");
    commonService.factory("CategoryRepository", categoryRepository);
    categoryRepository.$inject = ["$resource"];

    function categoryRepository($resource) {
        return $resource(apiBaseUrl + "api/", {}, {
            fetchAll: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "categorys"
            },
            fetch: {
                method: 'GET',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "category/:id"
            },
            save: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "category"
            },
            update: {
                method: 'PUT',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "category"
            },
            remove: {
                method: 'DELETE',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "category/:id"
            },
            removeAll: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "category/remove"
            }
        });
    }
}());
