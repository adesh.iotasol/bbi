(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("EventEditController", eventEditController);
    eventEditController.$inject = ["$scope", "$state", "$timeout", "EventRepository", "$filter", "FileUploader", "$stateParams", "AuthServerProvider"];

    function eventEditController($scope, $state, $timeout, AccessRepository, $filter, FileUploader, $stateParams, AuthRepository) {

        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.remove = remove;
        vm.type = '';
        vm.uploadFile = uploadFile;
        vm.removeEventImage = removeEventImage;
        vm.authRepository = AuthRepository;
        vm.initializingDatePicker = initializingDatePicker;
        vm.getMediaType = getMediaType;
        init();

        function init() {
            vm.record = {};
            vm.recordId = $stateParams.id;
            vm.isNewRecord = true;
            vm.record.isDeleted = false;
            vm.record.isActive = true;
            if (vm.recordId <= 0) {
                autoSelected();
                return;
            }
            AccessRepository.fetch({
                "id": vm.recordId
            }, function (response) {
                vm.loading = false;
                vm.isNewRecord = false;
                vm.record = response.data;
                if (vm.record.videoPath.length > 0) {
                    vm.mediaType = "video";
                    vm.type = "video";
                } else {
                    vm.mediaType = "image";
                    vm.type = "image";
                }
                postprocessing();

            },
            function (error) {
                vm.errorResponse = error.data;
                vm.isError = true;
                vm.loading = false;
            });
        }

        function getMediaType(type) {
            type == 'image' ? vm.record.videoPath = '' : vm.record.file = [];
            vm.type = type;
        }

        function initializingDatePicker(ele) {
            $(ele).datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true,
                startDate: new Date()
            });
        }

        function postprocessing() {
            autoSelected();
            $('.StartDate').datepicker('setDate', moment(vm.record.startDate).format('DD/MM/YYYY'));
            $('.EndDate').datepicker('setDate', moment(vm.record.endDate).format('DD/MM/YYYY'));
            setTimeout(function () {
            }, 300);
        }

        function uploadFile() {
            $("#fileUpload").click();
        }

        function autoSelected(value) {
        }

        function save(bol) {
            vm.onClickValidation = !bol;
            if (!bol) {
                return;
            }

            if (vm.type.length <= 0) {
                showTost("Error:", "Please select video or image option", "danger");
                return;
            }

            if ((vm.record.videoPath == '') && (!vm.record.file) && (vm.type == 'image')) {
                showTost("Error:", "Please upload image", "danger");
                return;
            }
            if (!vm.record.videoPath && vm.type == 'video') {
                showTost("Error:", "Please upload video", "danger");
                return;
            }

            if (!vm.record.videoPath ==  '') {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var url = vm.record.videoPath;
                var match = url.match(regExp);
                if (!match || !vm.record.videoPath.includes("youtu")) {
                    showTost("Error:", "Invalid Youtube Link", "danger");
                    return;
                }
                vm.record.videoPath = `https://www.youtube.com/embed/${match[2]}`;

            }

            var tempRecord = angular.copy(vm.record);
            tempRecord.startDate = moment(tempRecord.startDate, "DD/MM/YYYY").format("MM/DD/YYYY");
            tempRecord.endDate = moment(tempRecord.endDate, "DD/MM/YYYY").format("MM/DD/YYYY");
            if (tempRecord.startDate > tempRecord.endDate) {
                showTost("Error:", "End date shoud be greater than of start date", "danger");
                return;
            }

            var method = "save";
            if (!vm.isNewRecord) {
                method = "update"
            }

            AccessRepository[method](tempRecord, function (data) {
                if (!data.status) {
                    showTost("Error:", data.message, "danger");
                    return;
                }
                showTost("Success:", data.message, "success");
                vm.cancel();
            }, function (error) {
                showTost("Error:", error.data.message, "danger");
            });
        }

        function cancel() {
            $state.go("EventList");
        }

        function remove() {
            customDeleteConfirmation(removeRecordCallback, vm.recordId);
        }

        function removeRecordCallback(recordId) {
            AccessRepository.remove({
                id: recordId
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                showTost("Success:", "Record Deleted Successfully", "success");
                vm.cancel();
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }

        function removeEventImage() {
            customDeleteConfirmation(removeEventImageCallback, vm.record.file);
        }

        function removeEventImageCallback(obj) {
            if (!obj.id) {
                $scope.$apply(function () {
                    vm.record.file = {};
                    sweetAlert.close();
                    showTost("Success:", "Event Image Removed Successfully", "success");
                });
                return;
            }
            AccessRepository.removeEventImage({
                id: obj.id
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                vm.record.file = {};
                showTost("Success:", "Event Image Removed Successfully", "success");
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }

        vm.loadAssociatedPopup = loadAssociatedPopup;

        function loadAssociatedPopup(name, fieldName) {
            vm.associated = {
                isActive: true
            };
            vm.loadAssociatedAll(fieldName);
            $("#" + name).modal("show");

        }
        vm.onSelectAssociated = onSelectAssociated;

        function onSelectAssociated(value, name, popUpName) {
            vm.record[name] = value;
            setTimeout(function () {
                $('.' + name).val(value).trigger("change");
            });
            vm.associated = undefined;
            $("#" + popUpName).modal("hide");
        }

        vm.onRemoveAssociated = onRemoveAssociated;

        function onRemoveAssociated(id, fieldName) {
            customDeleteConfirmation(removeAssociatedRecordCallback, id, fieldName);
        }

        function removeAssociatedRecordCallback(recordId, fieldName) {
            vm.repository.remove({
                id: recordId
            }, function (response) {
                sweetAlert.close();
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                showTost("Success:", "Record Deleted Successfully", "success");
                vm.loadAssociatedAll(fieldName);
            }, function (error) {
                sweetAlert.close();
                showTost("Error:", "Some Things went wrong.", "danger");
            });
        }

        vm.file_uploader = new FileUploader({
            url: secureApiBaseUrl + 'file/group/items/upload',
            autoUpload: true
        });
        vm.file_uploader.onSuccessItem = function (fileItem, response, status, headers) {
            vm.record.file = response[0];
        };

        vm.loadAssociatedAll = loadAssociatedAll;

        function loadAssociatedAll(fieldName) {
            vm.repository.fetchAll(function (response) {
            });
        }
        vm.onSaveAssociated = onSaveAssociated;

        function onSaveAssociated(fieldName) {
            vm.repository.save(vm.associated, function (response) {
                if (!response.status) {
                    showTost("Error:", response.message, "danger");
                    return;
                }
                vm.associated = {
                    isActive: true
                };
                vm.showAddForm = false;
                vm.loadAssociatedAll(fieldName);
            });
        }
    }
}());

