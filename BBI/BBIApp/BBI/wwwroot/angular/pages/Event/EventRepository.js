(function () {
    var commonService = angular.module("common.services");
    commonService.factory("EventRepository", eventRepository);
    eventRepository.$inject = ["$resource"];

    function eventRepository($resource) {
        return $resource(apiBaseUrl + "api/", {}, {
            fetchAll: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "events"
            },

            activeEvents: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "active-event"
            },

            fetch: {
                method: 'GET',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "event/:id"
            },
            save: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "event"
            },
            update: {
                method: 'PUT',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "event"
            },
            remove: {
                method: 'DELETE',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "event/:id"
            },
            removeAll: {
                method: 'POST',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "event/remove"
            },
            removeEventImage: {
                method: 'DELETE',
                params: {
                    id: '@id'
                },
                isArray: false,
                url: secureApiBaseUrl + "file/group/item/delete/:id"
            }
        });
    }
}());
