﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("StoriesController", storiesController);
    storiesController.$inject = ["$filter", "StoriesRepository", "$q"];

    function storiesController($filter, StoriesRepository, $q) {

        var vm = this;
        vm.getCategoryWiseBlogs = getCategoryWiseBlogs;
        function init() {
            vm.records = [];
            vm.categorys = [];
            showLoader();
            $q.all([
              StoriesRepository.activeBlogs().$promise,
              StoriesRepository.categorys().$promise
            ]).then(function (responses) {
                $(window).scrollTop(0);
                vm.records = responses[0].data;
                vm.categorys = responses[1].data;
                hideLoader();
            },
          function (error) {
              //vm.errorResponse = error.data;
              hideLoader();
              return;
          });

        }

        function getCategoryWiseBlogs(id) {
            StoriesRepository.activeBlogs({ CategoryId: id }, function (response) {
                vm.records = response.data;
                $(window).scrollTop(0);
                console.log(1);
                hideLoader();
            });
        }
        init();
    }
}());