﻿(function () {
    var commonService = angular.module("common.services");
    commonService.factory("StoriesRepository", storiesRepository);
    storiesRepository.$inject = ["$resource"];

    function storiesRepository($resource) {
        return $resource(apiBaseUrl + "api/", {}, {
            categorys: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "categorys"
            },
            activeBlogs: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "active-blogs"
            },
            activeBlog: {
                method: 'GET',
                params: {
                    slug: '@slug'
                },
                isArray: false,
                url: secureApiBaseUrl + "active-blog/:slug"
            },
            homepageActive: {
                method: 'GET',
                params: {},
                isArray: false,
                url: secureApiBaseUrl + "homepage/active"
            },
        })
    }
}());