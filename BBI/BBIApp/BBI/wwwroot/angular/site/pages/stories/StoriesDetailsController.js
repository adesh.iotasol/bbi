﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("StoriesDetailsController", storiesDetailsController);
    storiesDetailsController.$inject = ["$stateParams", "$filter", "$q", "StoriesRepository"];

    function storiesDetailsController($stateParams, $filter, $q, StoriesRepository) {

        var vm = this;
        vm.getCategoryWiseBlogs = getCategoryWiseBlogs;
        function init() {
            vm.record = {};
            vm.categorys = [];
            vm.bloges = [];
            vm.filterBloges = false;
            showLoader();
            $q.all([
               StoriesRepository.activeBlog({ "slug": $stateParams.slug }).$promise,
               StoriesRepository.categorys().$promise
            ]).then(function (responses) {
                $(window).scrollTop(0);
                vm.record = responses[0].data;
                vm.categorys = responses[1].data;
                hideLoader();
            },
           function (error) {
               //vm.errorResponse = error.data;
               hideLoader();
               return;
           });
        }

        function getCategoryWiseBlogs(id) {
            StoriesRepository.activeBlogs({ CategoryId: id }, function (response) {
                vm.bloges = response.data;
                vm.filterBloges = true;
                hideLoader();
                $(window).scrollTop(0);

            });
        }
        init();
    }
}());