﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("CommunityController", communityController);
    communityController.$inject = ["$timeout", "$scope", "$filter", "EventRepository", "$state", "$stateParams"];

    function communityController($timeout, $scope, $filter, EventRepository, $state, $stateParams) {

        var vm = this;
        vm.stateChange = stateChange;
        function init() {
            vm.currentState = $state.current;
            vm.records = [];
            showLoader();
            EventRepository.activeEvents(function (response) {
                vm.events = response.data;
                hideLoader();
                if (vm.currentState.name == "EventListSection") {
                    $timeout(function () {
                        $(window).scrollTop($('#' + $stateParams.scrollTo).offset().top);
                    });
                }
            },
                function (error) {
                    hideLoader();
                    //vm.errorResponse = error.data;
                    return;
                });
        }

        function stateChange() {
            if (vm.currentState.name == "EventListSection") {

                $(window).scrollTop($('#events-section').offset().top);
            } else {
                $state.go("EventListSection", { scrollTo: 'events-section' })
            }
        }

        init();
    }
}());