﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("CommunityEventDetailsController", communityEventDetailsController);
    communityEventDetailsController.$inject = ["$filter", "$stateParams", "EventRepository"];

    function communityEventDetailsController($filter, $stateParams, EventRepository) {

        var vm = this;
        function init() {
            vm.event = {};
            showLoader();
            EventRepository.fetch({ id: $stateParams.id }, function (response) {
                vm.event = response.data;
                hideLoader();
            },
            function (error) {
                return;
            });
        }
        init();
    }
}());