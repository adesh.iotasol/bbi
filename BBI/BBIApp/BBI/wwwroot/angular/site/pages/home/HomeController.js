﻿(function () {
    var applicationApp = angular.module("applicationApp");
    applicationApp.controller("HomeController", homeController);
    homeController.$inject = ["$filter", "$stateParams", "StoriesRepository"];

    function homeController($filter, $stateParams, StoriesRepository) {

        var vm = this;
        function init() {
            vm.records = [];
            showLoader();
            StoriesRepository.homepageActive(function (response) {
                vm.records = response.data;
                hideLoader();
            },
    function (error) {
        //vm.errorResponse = error.data;
        hideLoader();
        return;
    });
        }
        init();
    }
}());