﻿//PreLoader Start
function hideLoader() {
    setTimeout(function () {
        $("#preloader").css("display", "none");
        $('body').delay(350).css({ 'overflow': 'visible' });
    })
}

function closeMenu() {
    $('.meanclose').click();
}
function showLoader() {
    $("#preloader").css("display", "block");
}
//PreLoader End

//Appoinment Wrapper Start
function sideSildre() {
    if ($('.header-right-link .slice-btn').length) {
        //Show Form
        $('.header-right-link .slice-btn').on('click', function (e) {
            e.preventDefault();
            $('body').addClass('visible-appointment');
        });
        //Hide Form
        $('.appointment-wrapper .appoint-box .cross-icon,.layer-drop').on('click', function (e) {
            e.preventDefault();
            $('body').removeClass('visible-appointment');
        });
    }
}
//Appoinment Wrapper End

//Project carousel Start
function ProjectCarousel() {
    var project_carousel = $('.project-carousel');
    project_carousel.owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        navText: ["<i class='icon icon-chevron-left'></i>", "<i class='icon icon-chevron-right'></i>"],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            700: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
}
//Project carousel End